@extends('layouts.master')

@section('style')
	@parent
	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  	<link rel="stylesheet" href="{{ asset('adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
  	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
@endsection

@section('content')
	<section class="content-header">
		<div class="container-fluid">
			<div class="container-fluid">
				<div class="row mb-2">
					<div class="col-sm-6">
						<h1 class="m-0 text-dark">Pilih linen untuk digunakan</h1>
					</div>
					<div class="col-sm-6">
						<ol class="breadcrumb float-sm-right">
							<li class="breadcrumb-item"><a href="/dashboard">Beranda</a></li>
							<li class="breadcrumb-item active">Gunakan Linen</li>
						</ol>
					</div>
				</div>
			</div>
		</div>
	</section>

	@if(Session::has('message'))
    <div class="alert">
        {!! Session::get('message') !!}
    </div>
	@endif
    
    <section class="content">
    	<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<div class="card card-primary card-outline">
						<div class="card-body">
							<table id="use-linen-list" class="table table-bordered table-hover" width="100%">
								<thead>
									<tr>
										<th width="20px">No</th>
										<th>Nomor Order</th>
										<th>Kode Linen</th>
										<th>Nama Linen</th>
										<th>Jumlah</th>
										<th width="50px"></th>
									</tr>
	                			</thead>
								<tbody>
								</tbody>								
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="modal fade" id="use-linen-modal">
	        <div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Cek kembali linen yang akan dipakai</h4>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
	            	</div>
	            	{!! Form::open(['url' => '/uselinen/use', 'id'=>'form-qty-use-linen']) !!}
		            	<div class="modal-body">
		            		{{ Form::hidden('modal_use_linen_order_id', '', ['id'=>'modal-use-linen-order-id']) }}
		            		{{ Form::hidden('modal_linen_line_id', '',['id'=>'modal-linen-line-id']) }}
		            		{{ Form::hidden('modal_max_qty', '', ['id'=>'modal-max-qty']) }}
		            		<div class="form-group row">
		            			<label class="col-sm-3 col-form-label">Kode Barang</label>
		            			<div class="col-sm-9">
		            				{{ Form::text('modal_linen_code', '', ['class'=>'form-control', 'id'=>'modal-linen-code', 'readonly']) }}
		            			</div>
		            		</div>
		            		<div class="form-group row">
		            			<label class="col-sm-3 col-form-label">Nama Barang</label>
		            			<div class="col-sm-9">
		            				{{ Form::text('modal_linen_name', '', ['class'=>'form-control', 'id'=>'modal-linen-name', 'readonly']) }}
		            			</div>
		            		</div>
		            	</div>

		            	<div class="modal-footer justify-content-between">
		            		<a href="#" class="btn btn-danger" data-dismiss="modal">Batal</a>
		            		<button class="btn btn-primary" id="submit-line">Pakai</button>
		            	</div>
	            	{!! Form::close() !!}
	        	</div>
	    	</div>
	    </div>

	    
	    <div class="modal fade" id="use-order-modal">
	        <div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Order Linen <span id="modal-order-no"></span></h4>
						<button type="button" class="close cancel-use-order" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
	            	</div>
	            	<div class="modal-body">
	            		{!! Form::open(['url' => '/useorder/use', 'id'=>'form-use-order']) !!}
	            		{{ Form::hidden('modal_order_id', '', ['id'=>'modal-order-id']) }}
	            		<label style="color: red">
	            			Pakai linen dalam 1 order ini ?
	            		</label>
	            		<table class="table table-condensed" id="ready-use-linen-table">
	            			<thead>
	            				<th>
	            					Kode Linen
	            				</th>
	            				<th>
	            					Nama Linen
	            				</th>
	            				<th></th>
	            			</thead>
	            			<tbody>
	            			</tbody>
	            		</table>
	            	</div>
	            	<div class="modal-footer justify-content-between">
	            		<a href="#" class="btn btn-danger cancel-use-order">Batal</a>
	            		{{ Form::submit('Pakai', array('class'=>'btn btn-primary')) }}
	            	</div>
	            	{{ Form::close() }}
	        	</div>
	    	</div>
	    </div>
	</section>
@endsection

@section('js')
	@parent
	<script src="{{ asset('adminlte/plugins/datatables/jquery.dataTables.js') }}"></script>
	<script src="{{ asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
	<script type="text/javascript" src="{{ asset('/js/use_linen.js') }}"></script>
@endsection

