@extends('layouts.master')

@section('style')
	@parent

@endsection

@section('content')
	{{ Form::hidden('edit_mode', $edit_mode, ['id'=>'edit-mode']) }}
	<section class="content-header">
		<div class="container-fluid">
			<div class="container-fluid">
				<div class="row mb-2">
					<div class="col-sm-6">
						<h1 class="m-0 text-dark">{{ $pagetitle }}</h1>
					</div>
					<div class="col-sm-6">
						<ol class="breadcrumb float-sm-right">
							<li class="breadcrumb-item"><a href="/dashboard">Beranda</a></li>
							<li class="breadcrumb-item"><a href="/orderlinen">Order Linen</a></li>
							<li class="breadcrumb-item active">{{$pagetitle}}</li>
						</ol>
					</div>
				</div>
			</div>
		</div>
	</section>

	@if(Session::has('message'))
    <div class="alert">
        {!! Session::get('message') !!}
    </div>
	@endif

	<section class="content">
    	<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<div class="card card-primary card-outline">
						<div class="card-header">
							<div class="row col-12">
								@if ($id)
									<div class="row col-6">
									@if ($status == 'draft')
										<div class="col-2" id="div-cancel-btn">
											{{ Form::button('Batal', ['class'=>'btn btn-block btn-danger', 'id'=>'cancel-edit-btn']) }}
										</div>
										<div class="col-2">
											{{ Form::button('Edit', ['class'=>'btn btn-block btn-default', 'id'=>'do-edit-btn']) }}
										</div>
										@if(Auth::user()->role == 'superadmin' || Auth::user()->role == 'admin_linen' )
										<div class="col-2">
											<div style="width: 90px">
												{{ Form::button('Setujui', ['class'=>'btn btn-block btn-success', 'id'=>'approve-btn']) }}
											</div>
										</div>
										@endif
									@endif
									</div>
									<div class="col-6">
										<h4 ><span class="badge badge-pill badge-info float-sm-right">{{ $status_string }}</span></h4>
									</div>
								@else
									<div style="width: 90px">
										<a href="/orderlinen" class="btn btn-block btn-danger"> Batal</a>
									</div>						
								@endif
							</div>
						</div>

						<div class="card-body">
						{!! Form::open(['url' => '/orderlinen/store', 'id'=>'form-order-linen']) !!}
							<div class="card-body">
								{{ Form::hidden('id', $id, ['id'=>'id']) }}
								@if($id)
									<div class="form-group row">
	                    				<label class="col-sm-2 col-form-label">Nomor Order</label>
	                    				<div class="col-sm-10">
	                    					{{ Form::text('order_no', $order_no, ['class'=>'form-control', 'id'=>'order-no', 'readonly']) }}
	                    				</div>
	                    			</div>
                    			@endif

                    			<div class="form-group row">
                    				<label class="col-sm-2 col-form-label">Ruangan</label>
                    				<div class="col-sm-10">
                    					{{ Form::select('room_id', $room_list, $room_id, ['class'=>'form-control select2', 'id'=>'room-id', 'required']) }}
                    				</div>
                  				</div>
                  				
                    			<div class="form-group row">
                    				<label class="col-sm-2 col-form-label">Tanggal Order</label>
                    				<div class="col-sm-10 input-group">
                    					<div class="input-group">
                    						<div class="input-group-prepend">
	                      						<span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
	                    					</div>
	                    					{{ Form::text('order_date', $order_date, ['class'=>'form-control datepicker', 'id'=>'order-date', 'required']) }}
										</div>
                    				</div>
                    			</div>

                  				@if($id)
	                  				<div class="form-group row">
	                    				<label class="col-sm-2 col-form-label">Diorder oleh</label>
	                    				<div class="col-sm-10">
	                      					<input name="ordered_by" class="form-control" id="ordered-by" value="{{ $ordered_by }}" readonly>
	                    				</div>
	                  				</div>
	                  				@if ($approved_by)
		                  				<div class="form-group row">
		                    				<label class="col-sm-2 col-form-label">Disetujui oleh</label>
		                    				<div class="col-sm-10">
		                      					<input name="approved_by" class="form-control" id="approved-by" value="{{ $approved_by }}" readonly>
		                    				</div>
		                  				</div>
	                  				@endif

	                  				<div class="form-group row">
	                    				<label class="col-sm-2 col-form-label">Total Linen</label>
	                    				<div class="col-sm-10">
	                      					<input name="total_linen" class="form-control" id="total-linen" value="{{ $total_linen }}" readonly>
	                    				</div>
	                  				</div>
                  				@endif
                  			</div>

							<div class="card-body">
								<ul class="nav nav-tabs" id="custom-content-below-tab" role="tablist">
			                  		<li class="nav-item">
			                    		<a class="nav-link active" id="custom-tabs-one-home-tab" data-toggle="pill" href="#detail-order" role="tab" aria-controls="custom-tabs-one-home" aria-selected="true"> Detail Linen</a>
			                  		</li>
			                	</ul>
								<div class="tab-content" id="detail-order">
									<div class="tab-pane fade show active" id="custom-tabs-one-home" role="tabpanel" aria-labelledby="custom-tabs-one-home-tab">
										<div class="row">
											<div class="card-body">
												<table class="table table-condensed" id="order-linen-line">
													<thead>
											            <tr>
															<th> Kode Linen</th>
															<th> Nama Linen</th>
															<th> Jumlah</th>
															<th style="width: 20px" class="action-col"></th>
											            </tr>
													</thead>
													<tbody id="order-line-body">
														<!--  -->
									                </tbody>
									                <tfooter>
									                	<tr>
									                		<td colspan="4">
									                			<button type="button" class="btn btn-default" data-toggle="modal" data-target="#add-linen-modal" id="add-linen-btn">
                  													+ Linen
                												</button>
									                		</td>
									                	</tr>
									                </tfooter>
									           	</table>
										    </div>
			                  			</div> 
           							</div>
           						</div>
           					</div>
           						
           					<div class="card-body">
                  				<div class="row">
                  					<div class="col-6">
                  						{{ Form::submit('Simpan', array('class'=>'btn btn-primary','accesskey'=>'s', 'id'=>'order-submit-btn')) }}
                  					</div>
	                  				<div class="col-6">
	                  					@if ($id)
	                  						{{ Form::button('<i class="fas fa-print"></i> <u>P</u>rint', array('class'=>'btn btn-warning text-white float-right print-order','accesskey'=>'p')) }}
	                  					@endif
	                  				</div>
	                  				
                  				</div>
                  				<div class="form-group row">
                  					
                  				</div>
                  			</div>
						{{ Form::close() }}
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<div class="modal fade" id="add-linen-modal">
        <div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Tambah Linen</h4>
					<button type="button" class="close cancel-submit-line" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
            	</div>
            	<div class="modal-body">
            		{{ Form::hidden('line_id', '', ['id'=>'line-id']) }}
            		<div class="form-group row">
						<label class="col-sm-2 col-form-label">Pilih Linen</label>
						<div class="col-sm-10">
							{{ Form::select('linen_id', $linen_list, '', ['class'=>'form-control select2', 'id'=>'linen-id', 'required']) }}
						</div>
					</div>
					
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Kode Linen</label>
						<div class="col-sm-10">
							{{ Form::text('linen_code', '', ['class'=>'form-control', 'id'=>'linen-code', 'readonly']) }}
						</div>
					</div>

					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Nama Linen</label>
						<div class="col-sm-10">
							{{ Form::text('linen_name', '', ['class'=>'form-control', 'id'=>'linen-name', 'readonly']) }}
						</div>
					</div>

            	</div>
            	<div class="modal-footer justify-content-between">
            		<button class="btn btn-danger cancel-submit-line" data-dismiss="modal">Batal</button>
            		<button class="btn btn-primary" id="submit-line">Simpan</button>
            	</div>
        	</div>
    	</div>
    </div>
@endsection
	
@section('js')
	@parent
	<script src="{{ asset('/js/order_linen.js') }}"></script>

@endsection