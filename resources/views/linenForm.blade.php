@extends('layouts.master')

@section('style')
	@parent
	
@endsection

@section('content')
	<section class="content-header">
		{{ Form::hidden('edit_mode', $edit_mode, ['id'=>'edit-mode']) }}
		<div class="container-fluid">
			<div class="container-fluid">
				<div class="row mb-2">
					<div class="col-sm-6">
						<h1 class="m-0 text-dark">{{ $pagetitle }}</h1>
					</div>
					<div class="col-sm-6">
						<ol class="breadcrumb float-sm-right">
							<li class="breadcrumb-item"><a href="/dashboard">Beranda</a></li>
							<li class="breadcrumb-item"><a href="/linen">Linen</a></li>
							<li class="breadcrumb-item active">{{$pagetitle}}</li>
						</ol>
					</div>
				</div>
			</div>
		</div>
	</section>
    
    <section class="content">
    	<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<div class="card card-primary card-outline">
						<div class="card-header">
							<div class="row col-12">
								@if ($id)
									<div class="col-1" id="div-cancel-btn">
										{{ Form::button('Batal', ['class'=>'btn btn-block btn-danger', 'id'=>'cancel-edit-btn']) }}
									</div>
									<div class="col-1">
										{{ Form::button('Edit', ['class'=>'btn btn-block btn-default', 'id'=>'do-edit-btn']) }}
									</div>
								@else
									<div style="width: 90px">
										<a href="/linen" class="btn btn-block btn-danger"> Batal</a>
									</div>						
								@endif
							</div>
						</div>

						<div class="card-body">
              @if ($id)
              @if($date_diff > 730)
              <div class="ribbon-wrapper ribbon-xl">
                <div class="ribbon bg-warning text-lg">
                  HABIS MASA PAKAI
                </div>
              </div>
              @endif
              @endif
							<form method="POST" action="/linen/store">
								@csrf
								<input type="hidden" id="id" name="id" value="{{ $id }}">
								<div class="form-group row">
                    				<label class="col-sm-2 col-form-label">Kode Linen</label>
                    				<div class="col-sm-10">
                      					{{ Form::text('linen_code', $linen_code, ['class'=>'form-control', 'id' => 'linen-code', 'required', 'autocomplete'=>'off']) }}
                    				</div>
                    			</div>

                          <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Kode Grup</label>
                            <div class="col-sm-10">
                                {{ Form::select('group_id', $group_list, $group_id, ['class'=>'form-control select2', 'id' => 'group-id', 'required']) }}
                            </div>
                          </div>

								          <div class="form-group row">
                    				<label class="col-sm-2 col-form-label">Nama Linen</label>
                    				<div class="col-sm-10">
                      					{{ Form::text('linen_name', $linen_name, ['class'=>'form-control', 'id' => 'linen-name', 'required', 'readonly']) }}
                    				</div>
                    			</div>
                    			
                    			<div class="form-group row">
                    				<label class="col-sm-2 col-form-label">Ruangan</label>
                    				<div class="col-sm-10">
                      					{{ Form::select('room_id', $room_list, $room_id, ['class'=>'form-control select2', 'id'=>'room-id']) }}
                    				</div>
                    			</div>
                    			
                  				<div class="form-group row">
                    				<label class="col-sm-2 col-form-label">Tanggal Pembelian</label>
                    				<div class="col-sm-10 input-group">
                    					<div class="input-group-prepend">
                      						<span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                    					</div>
                    					{{ Form::text('procurement_date', $procurement_date, ['class'=>'form-control datepicker', 'id'=>'procurement-date', 'required']) }}
                    				</div>
                  				</div>

                  				<div class="form-group row">
                    				<label class="col-sm-2 col-form-label">Satuan</label>
                    				<div class="col-sm-10">
                    					{{ Form::text('uom', 'pcs', ['class'=>'form-control', 'id'=>'uom', 'readonly']) }}
                    				</div>
                  				</div>

                  				@if($id)
                  				<div class="form-group row">
                    				<label class="col-sm-2 col-form-label">Umur Linen</label>
                    				<div class="col-sm-10">
                    					{{ Form::text('linen_age', $linen_age, ['class'=>'form-control', 'id'=>'linen-age', 'readonly']) }}
                    				</div>
                  				</div>

                  				<div class="form-group row">
                    				<label class="col-sm-2 col-form-label">Penggunaan</label>
                    				<div class="col-sm-10">
                    					{{ Form::number('frequencies_number', ($frequencies_number) ? $frequencies_number : 0, ['class'=>'form-control', 'id'=>'frequencies-number', 'readonly']) }}
                    				</div>
                  				</div>

                  				<div class="form-group row">
                    				<label class="col-sm-2 col-form-label">Sedang Digunakan</label>
                    				<div class="col-sm-10">
                    					{{ Form::text('in_use', ($in_use > 0) ? "Ya":"Tidak", ['class'=>'form-control', 'id'=>'in-use', 'readonly']) }}
                    				</div>
                  				</div>

                          <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Aktif Linen</label>
                            <div class="col-sm-10">
                              <div class="input-group-prepend">
                                  <span class="input-group-text">
                                    {{ Form::checkbox('active', 1, $active, ['class'=>'','id'=>'active']) }}
                                  </span>
                                </div>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Rusak</label>
                            <div class="col-sm-10">
                              <div class="input-group">
                                <div class="input-group-prepend">
                                  <span class="input-group-text">
                                    {{ Form::checkbox('broken', 1, $broken, ['class'=>'','id'=>'broken']) }}
                                  </span>
                                </div>
                                {{ Form::text('broken_reason', $broken_reason, ['class'=>'form-control', 'id'=>'broken-reason']) }}
                              </div>
                            </div>
                          </div>
                  				@endif

                  				<div class="form-group row">
	                  				<button type="submit" class="btn btn-block bg-gradient-primary col-1" id="submit-linen-btn">Simpan</button>
                  				</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>

	</section>
@endsection

@section('js')
	@parent
	<script type="text/javascript" src="/js/linen.js"></script>
@endsection