<html>
<head>
    <title>Daftar Linen yang rusak</title>
<style type="text/css">

</style>
</head>
<body>
    <table id="broken-list" class="display" border="1">
        <thead>
            <tr>
                <th>No</th>
                <th>Kode Linen</th>
                <th>Nama Linen</th>
                <th>Penyebab Rusak</th>
                <th>Satuan</th>
            </tr>
        </thead>
        <tbody>
            @foreach($broken_linen as $bl)
            <tr>
                <td>{{ $bl->no }}</td>
                <td>{{ $bl->linen_code }}</td>
                <td>{{ $bl->linen_name }}</td>
                <td>{{ $bl->broken_reason }}</td>
                <td>{{ $bl->uom }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>