@extends('layouts.master')

@section('style')
	@parent
	<link rel="stylesheet" href="adminlte/plugins/fontawesome-free/css/all.min.css">
 	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  	<link rel="stylesheet" href="adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
  	<link rel="stylesheet" href="adminlte/dist/css/adminlte.min.css">
  	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
@endsection

@section('content')
	<section class="content-header">
		<div class="container-fluid">
			<div class="container-fluid">
				<div class="row mb-2">
					<div class="col-sm-6">
						<h1 class="m-0 text-dark">Pengguna</h1>
					</div>
					<div class="col-sm-6">
						<ol class="breadcrumb float-sm-right">
							<li class="breadcrumb-item"><a href="/dashboard">Beranda</a></li>
							<li class="breadcrumb-item active">Pengguna</li>
						</ol>
					</div>
				</div>
			</div>
		</div>
	</section>

	@if(Session::has('message'))
    <div class="alert">
        {!! Session::get('message') !!}
    </div>
	@endif
    
    <section class="content">
    	<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<div class="card card-primary card-outline">
						<div class="card-header">
							<div style="width: 110px">
								<a href="/user/create" class="btn btn-block bg-gradient-secondary"> + Pengguna</a>	
							</div>
							
						</div>
						<div class="card-body">
							<table id="user-list" class="table table-bordered table-hover" width="100%">
								<thead>
									<tr>
										<th width="10px" align="center">No</th>
										<th>Nama Pengguna</th>
										<th>Username</th>
										<th>Posisi</th>
										<th width="70px"></th>
									</tr>
	                			</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection

@section('js')
	@parent
	<script src="adminlte/plugins/jquery/jquery.min.js"></script>
	<script src="adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="adminlte/plugins/datatables/jquery.dataTables.js"></script>
	<script src="adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
	<script src="adminlte/dist/js/adminlte.min.js"></script>
	<script src="adminlte/dist/js/demo.js"></script>
	<script src="{{ asset('/js/user.js') }}"></script>
	
@endsection