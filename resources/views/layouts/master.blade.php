<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>{{ config('app.name', 'Laravel') }}</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="icon" type="image/png" href="/images/favicon.png"/>
  @section('style')
  <link rel="stylesheet" href="{{ asset('adminlte/plugins/fontawesome-free/css/all.min.css') }}">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="{{ asset('adminlte/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('adminlte/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}"> 
  <link rel="stylesheet" href="{{ asset('adminlte/plugins/select2/css/select2.min.css') }}">
  <link rel="stylesheet" href="{{ asset('adminlte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('adminlte/dist/css/adminlte.min.css') }}">
  <link rel="stylesheet" href="{{ asset('adminlte/plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
  <!-- <link rel="stylesheet" href="{{ asset('adminlte/plugins/daterangepicker/daterangepicker.css') }}"> -->
  <link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker.min.css') }}">
  <link rel="stylesheet" href="{{ asset('adminlte/plugins/summernote/summernote-bs4.css') }}">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link rel="stylesheet" href="{{ asset('css/global.css') }}">
  @show
  <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">
<div id="loading">
      <img src="/images/loader.gif" width="150px" /><br/><br/>
      Mohon tunggu...
    </div>
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
      
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Messages Dropdown Menu -->
      <li class="nav-item">

        <a class="btn btn-default" href="{{ route('logout') }}"
            onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">
            <i class="fas fa-sign-out-alt"></i>
            {{ __('Keluar') }}
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
          @csrf
        </form>
        </a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/dashboard" class="brand-link">
      <img src="{{ asset('images/favicon.png') }}" alt="Admin Linen Logo" class="brand-image img-circle elevation-3"
           style="opacity: .9">
      <span class="brand-text font-weight-light">Sentralisasi Linen</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{ asset('adminlte/dist/img/usericon.png') }}" class="" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{ Auth::user()->name }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          @if (Auth::user()->role == 'superadmin' || Auth::user()->role == 'admin_linen')
          <li class="nav-item has-treeview {!! (request()->is('room*') || request()->is('linen*') || request()->is('grouplinen*')) ? 'menu-open' : '' !!}">
            <a href="#" class="nav-link {!! (request()->is('room*') || request()->is('linen*') || request()->is('grouplinen*')) ? 'active' : '' !!}">
              <i class="nav-icon fas fa-database"></i>
              <p>
                Master Data
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/room" class="nav-link {!! (request()->is('room*')) ? 'active' : '' !!}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Ruangan </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/grouplinen" class="nav-link {!! (request()->is('grouplinen*')) ? 'active' : '' !!}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Grup Linen </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/linen" class="nav-link {!! (request()->is('linen*')) ? 'active' : '' !!}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Linen </p>
                </a>
              </li>
              
            </ul>
          </li>
          @endif
          <li class="nav-item has-treeview {!! (request()->is('orderlinen*') || request()->is('uselinen*') || request()->is('useorder*') || request()->is('washorder*') || request()->is('washlinen*') || request()->is('cleanlinen*') ) ? 'menu-open' : '' !!}">
            <a href="#" class="nav-link {!! (request()->is('orderlinen*')  || request()->is('uselinen*') || request()->is('useorder*') || request()->is('washorder*') || request()->is('washlinen*') || request()->is('cleanlinen*')) ? 'active' : '' !!}">
              <i class="nav-icon fas fa-share-alt"></i>
              <p>
                Distribusi Linen
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              @if (Auth::user()->role == 'superadmin' || Auth::user()->role == 'admin_room' || Auth::user()->role == 'admin_linen')
              <li class="nav-item">
                <a href="/orderlinen" class="nav-link {!! (request()->is('orderlinen*')) ? 'active' : '' !!}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Order Linen</p>
                </a>
              </li>
              @endif
              @if (Auth::user()->role == 'superadmin' || Auth::user()->role == 'admin_room')
              <li class="nav-item">
                <a href="/uselinen" class="nav-link {!! (request()->is('uselinen*')) ? 'active' : '' !!}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Linen Siap Pakai</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/washlinen" class="nav-link {!! (request()->is('washlinen*')) ? 'active' : '' !!}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Linen Kotor</p>
                </a>
              </li>
              @endif
              @if (Auth::user()->role == 'superadmin' || Auth::user()->role == 'admin_linen')
              <li class="nav-item">
                <a href="/cleanlinen" class="nav-link {!! (request()->is('cleanlinen*')) ? 'active' : '' !!}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Linen Bersih</p>
                </a>
              </li>
              @endif
            </ul>
          </li>
          @if (Auth::user()->role == 'superadmin' || Auth::user()->role == 'admin_linen' || Auth::user()->role == 'admin_room')
          <li class="nav-item has-treeview {!! (request()->is('report*')) ? 'menu-open' : '' !!}">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-copy"></i>
              <p>
                Summary
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/report/stock" class="nav-link {!! (request()->is('report/stock*')) ? 'active' : '' !!}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Stok</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/report/broken" class="nav-link {!! (request()->is('report/broken*')) ? 'active' : '' !!}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Linen Rusak</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/report/uselinen" class="nav-link {!! (request()->is('report/uselinen*')) ? 'active' : '' !!}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Jumlah Digunakan</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/report/washlinen" class="nav-link {!! (request()->is('report/washlinen*')) ? 'active' : '' !!}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Jumlah Dicuci</p>
                </a>
              </li>
            </ul>
          </li>
          @endif
          @if(Auth::user()->role == 'superadmin')
          <li class="nav-item has-treeview {!! (request()->is('user*')) ? 'menu-open' : '' !!}">
            <a href="#" class="nav-link {!! (request()->is('user*')) ? 'active' : '' !!}">
              <i class="nav-icon fas fa-cog"></i>
              <p>
                Pengaturan
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/user" class="nav-link {!! (request()->is('user*')) ? 'active' : '' !!} ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Pengguna</p>
                </a>
              </li>
            </ul>
          </li> 
          @endif
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <div class="content-wrapper">
    @yield('content')
  </div>
  
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2019 RS Harapan Magelang.</strong>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

@section('js')
<!-- jQuery -->
<script src="{{ asset('adminlte/plugins/jquery/jquery.min.js') }} "></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('adminlte/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{ asset('adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- ChartJS -->
<script src="{{ asset('adminlte/plugins/chart.js/Chart.min.js') }}"></script>
<!-- Sparkline -->
<script src="{{ asset('adminlte/plugins/sparklines/sparkline.js') }}"></script>
<script src="{{ asset('adminlte/plugins/select2/js/select2.full.min.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('adminlte/plugins/jquery-knob/jquery.knob.min.js') }}"></script>
<!-- daterangepicker -->
<script src="{{ asset('adminlte/plugins/moment/moment.min.js') }}"></script>
<!-- <script src="{{ asset('adminlte/plugins/daterangepicker/daterangepicker.js') }}"></script> -->
<script src="{{ asset('js/moment-with-locales.js') }}"></script>
<script src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{ asset('adminlte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
<!-- Summernote -->
<script src="{{ asset('adminlte/plugins/summernote/summernote-bs4.min.js') }}"></script>
<!-- overlayScrollbars -->
<script src="{{ asset('adminlte/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('adminlte/dist/js/adminlte.js') }}"></script>
<script src="{{ asset('js/global.js') }}"></script>

@show
</body>
</html>
