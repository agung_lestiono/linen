@extends('layouts.master')

@section('style')
	@parent
	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  	<link rel="stylesheet" href="{{ asset('adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
  	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
@endsection

@section('content')
	<section class="content-header">
		<div class="container-fluid">
			<div class="container-fluid">
				<div class="row mb-2">
					<div class="col-sm-6">
						<h1 class="m-0 text-dark">Gunakan Linen per Order {{ Auth::user()->role }}</h1>
					</div>
					<div class="col-sm-6">
						<ol class="breadcrumb float-sm-right">
							<li class="breadcrumb-item"><a href="/dashboard">Beranda</a></li>
							<li class="breadcrumb-item active">Gunakan order</li>
						</ol>
					</div>
				</div>
			</div>
		</div>
	</section>

	@if(Session::has('message'))
    <div class="alert">
        {!! Session::get('message') !!}
    </div>
	@endif
    
    <section class="content">
    	<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<div class="card card-primary card-outline">
						<div class="card-body">
							<table id="use-order-list" class="table table-bordered table-hover" width="100%">
								<thead>
									<tr>
										<th width="20px">No</th>
										<th>Nomor Order</th>
										<th>Tanggal Order</th>
										
										<th width="50px"></th>
									</tr>
	                			</thead>
								<tbody>
								</tbody>								
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="modal fade" id="use-order-modal">
	        <div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Masukan jumlah yang digunakan</h4>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
	            	</div>
	            	{!! Form::open(['url' => '/useorder/use', 'id'=>'form-qty-use-order']) !!}
		            	<div class="modal-body">
		            		{{ Form::hidden('modal_order_line_id', '',['id'=>'modal-order-line-id']) }}
		            		{{ Form::hidden('modal_max_qty', '', ['id'=>'modal-max-qty']) }}
		            		<div class="form-group row">
		            			<label class="col-sm-3 col-form-label">Kode Barang</label>
		            			<div class="col-sm-9">
		            				{{ Form::text('modal_order_code', '', ['class'=>'form-control', 'id'=>'modal-order-code', 'readonly']) }}
		            			</div>
		            		</div>
		            		<div class="form-group row">
		            			<label class="col-sm-3 col-form-label">Nama Barang</label>
		            			<div class="col-sm-9">
		            				{{ Form::text('modal_order_name', '', ['class'=>'form-control', 'id'=>'modal-order-name', 'readonly']) }}
		            			</div>
		            		</div>
		            		<div class="form-group row">
								<label class="col-sm-3 col-form-label">Jumlah</label>
								<div class="col-sm-9">
									{{ Form::number('modal_order_qty', '', ['class'=>'form-control', 'id'=>'modal-order-qty', 'min'=>1]) }}
								</div>
							</div>
		            	</div>

		            	<div class="modal-footer justify-content-between">
		            		<button class="btn btn-danger" data-dismiss="modal">Batal</button>
		            		<button class="btn btn-primary" id="submit-line">Simpan</button>
		            	</div>
	            	{!! Form::close() !!}
	        	</div>
	    	</div>
	    </div>
	</section>
@endsection

@section('js')
	@parent
	<script src="{{ asset('adminlte/plugins/datatables/jquery.dataTables.js') }}"></script>
	<script src="{{ asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
	<script type="text/javascript" src="{{ asset('/js/use_order.js') }}"></script>
@endsection

