@extends('layouts.master')

@section('style')
	@parent
  	<link rel="stylesheet" href="adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
  	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

@endsection

@section('content')
	<section class="content-header">
		<div class="container-fluid">
			<div class="container-fluid">
				<div class="row mb-2">
					<div class="col-sm-6">
						<h1 class="m-0 text-dark">Order Linen</h1>
					</div>
					<div class="col-sm-6">
						<ol class="breadcrumb float-sm-right">
							<li class="breadcrumb-item"><a href="/dashboard">Beranda</a></li>
							<li class="breadcrumb-item active">Order Linen</li>
						</ol>
					</div>
				</div>
			</div>
		</div>
	</section>

	@if(Session::has('message'))
    <div class="alert">
        {!! Session::get('message') !!}
    </div>
	@endif
    
    <section class="content">
    	<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<div class="card card-primary card-outline">
						<div class="card-header">
							<div class="col-2">
								<a href="/orderlinen/create" class="btn btn-block bg-gradient-secondary"> + Order Linen</a>	
							</div>
						</div>
						<div class="card-body">
							<table id="order-list" class="table table-bordered table-hover" width="100%">
								<thead>
									<tr>
										<th width="20px">No</th>
										<th>Nomor Order</th>
										<th>Tanggal Order</th>
										<th>Ruangan</th>
										<th>Diorder oleh</th>
										<th>Status</th>
										<th width="65"></th>
									</tr>
	                			</thead>
								<tbody>
								</tbody>								
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection

@section('js')
	@parent
	<script src="adminlte/plugins/datatables/jquery.dataTables.js"></script>
	<script src="adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
	<script src="adminlte/dist/js/adminlte.min.js"></script>
	<script src="adminlte/dist/js/demo.js"></script>
	<script src="{{ asset('/js/order_linen.js') }}"></script>

@endsection