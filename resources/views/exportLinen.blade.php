<html>
<head>
    <title>Persediaan Linen</title>
<style type="text/css">

</style>
</head>
<body>

    <table id="stock-list" class="display" border="1">
    @if($type == 'group')
        <thead>
            <tr>
                <th>No</th>
                <th>Nama </th>
                <th>Jumlah Stok</th>
            </tr>
        </thead>
        <tbody>
            @foreach($stock_data as $sd)
            <tr>
                <td>{{ $sd->no }}</td>
                <td>{{ $sd->linen_name }}</td>
                <td>{{ $sd->stock_qty }}</td>
            </tr>
            @endforeach
        </tbody>
    @else
        <thead>
            <tr>
                <th>No</th>
                <th>Kode Asset</th>
                <th>Nama </th>
                <th>Jumlah Stok</th>
            </tr>
        </thead>
        <tbody>
            @foreach($detail_stock_data as $dsd)
            <tr>
                <td>{{ $dsd->no }}</td>
                <td>{{ $dsd->linen_code }}</td>
                <td>{{ $dsd->linen_name }}</td>
                <td>{{ $dsd->qty }}</td>
            </tr>
            @endforeach
        </tbody>
    @endif
    </table>
</body>
</html>