@extends('layouts.master')

@section('style')
	@parent
	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  	<link rel="stylesheet" href="{{ asset('adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
  	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
@endsection 

@section('content')
	<section class="content-header">
		<div class="container-fluid">
			<div class="container-fluid">
				<div class="row mb-2">
					<div class="col-sm-6">
						<h1 class="m-0 text-dark">Persediaan Linen</h1>
					</div>
					<div class="col-sm-6">
						<ol class="breadcrumb float-sm-right">
							<li class="breadcrumb-item"><a href="/dashboard">Beranda</a></li>
							<li class="breadcrumb-item active">Persediaan Linen</li>
						</ol>
					</div>
				</div>
			</div>
		</div>
	</section>
    
    <section class="content">
    	<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<div class="card card-primary card-outline">
						<div class="card-header">
							<div class="col-2">
								<a href="#choose-template-stock" class="btn btn-block bg-gradient-warning" data-toggle="modal"> Export Excel</a>	
							</div>
							
						</div>
						<div class="card-body">
							<table id="stock-list" class="table table-bordered table-hover" width="100%">
								<thead>
									<tr>
										<th width="20px">No</th>
										<th>Nama</th>
										<th>Jumlah</th>
										<th>Satuan</th>
									</tr>
	                			</thead>
								<tbody>
								</tbody>								
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<div class="modal fade" id="choose-template-stock">
        <div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Export dalam format grup atau per linen?</h4>
					<button type="button" class="close cancel-submit-line" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
            	</div>
            	<div class="modal-body">
            		<div class="form-group row">
						<label class="col-sm-2 col-form-label">Pilih Format</label>
						<div class="col-sm-10">
							{{ Form::select('template_stock', [
								"group" => "Grup Linen",
								"detail" => "Detail per Linen"
								], '', ['class'=>'form-control select2', 'id'=>'template-stock', 'required']) }}
						</div>
					</div>
            	</div>
            	<div class="modal-footer justify-content-between">
            		<button class="btn btn-danger cancel-submit-line" data-dismiss="modal">Batal</button>
            		<button class="btn btn-primary" id="export-stock">Export</button>
            	</div>
        	</div>
    	</div>
    </div>
@endsection 

@section('js')
	@parent 
	<script src="{{ asset('adminlte/plugins/datatables/jquery.dataTables.js') }}"></script>
	<script src="{{ asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
	<script type="text/javascript" src="{{ asset('/js/report.js') }}"></script>
@endsection