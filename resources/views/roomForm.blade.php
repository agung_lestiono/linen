@extends('layouts.master')

@section('style')
	@parent
	
@endsection

@section('content')
	<section class="content-header">
		<div class="container-fluid">
			<div class="container-fluid">
				<div class="row mb-2">
					<div class="col-sm-6">
						<h1 class="m-0 text-dark">{{ $pagetitle }}</h1>
					</div>
					<div class="col-sm-6">
						<ol class="breadcrumb float-sm-right">
							<li class="breadcrumb-item"><a href="/dashboard">Beranda</a></li>
							<li class="breadcrumb-item"><a href="/room">Lokasi</a></li>
							<li class="breadcrumb-item active">{{ $pagetitle }}</li>
						</ol>
					</div>
				</div>
			</div>
		</div>
	</section>
    
    <section class="content">
    	{{ Form::hidden('edit_mode', $edit_mode, ['id'=>'edit-mode']) }}
    	<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<div class="card card-primary card-outline">
						<div class="card-header">
							<div class="row col-12">
								@if ($id)
									<div class="col-1" id="div-cancel-btn">
										{{ Form::button('Batal', ['class'=>'btn btn-block btn-danger', 'id'=>'cancel-edit-btn']) }}
									</div>
									<div class="col-1">
										{{ Form::button('Edit', ['class'=>'btn btn-block btn-default', 'id'=>'do-edit-btn']) }}
									</div>
								@else
									<div style="width: 90px">
										<a href="/room" class="btn btn-block btn-danger"> Batal</a>
									</div>						
								@endif
							</div>
						</div>

						<div class="card-body">
							<form method="POST" action="/room/store">
								@csrf
								<input type="hidden" id="id" name="id" value="{{ $id }}">
								<div class="form-group row">
                    				<label class="col-sm-2 col-form-label">Nama Ruangan</label>
                    				<div class="col-sm-10">
                      					{{ Form::text('name', $name, ['class'=>'form-control', 'id'=>'name', 'required']) }}
                    				</div>
                    			</div>

                    			<div class="form-group row">
                    				<label class="col-sm-2 col-form-label">Petugas</label>
                    				<div class="col-sm-10">
                      					{{ Form::select('officer_id', $officer_list, $officer_id, ['class'=>'form-control select2', 'id'=>'officer-id']) }}
                    				</div>
                    			</div>

                  				<div class="form-group row">
	                  				<button type="submit" class="btn btn-block bg-gradient-primary col-1" id="submit-room-btn">Simpan</button>
	                  				
                  				</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection

@section('js')
	@parent
	<script src="{{ asset('/js/room.js')}}"></script>
@endsection