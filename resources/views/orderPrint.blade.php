<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" CONTENT="noindex,nofollow">
	<title>{{ $order_no }}</title>
</head>
<body>
    <table cellpadding="10" width="100%">
        <tr>
            <td width="50%">                
                <table width="100%">
                    <tr>
                        <td colspan="2">
                            <h2>RS HARAPAN MAGELANG</h2>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="font-size: 16px">
                            Jl. Panembahan Senopati No. 11
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="font-size: 16px">Magelang</td>
                    </tr>
                    <tr>
                        <td colspan="2" style="font-size: 16px">Telp. 0298 364033</td>
                    </tr>
                </table>
                <div class="gap"></div>
                <hr>
                <table width="100%">
                    <tr>
                        <td align="center" style="font-size: 20px">ORDER LINEN</td>
                    </tr>
                </table>
                <hr>
                <div class="gap"></div>
                <table width="100%">
                    <tr>
                        <td width="30%">
                            Tanggal Order      
                        </td>
                        <td width="50%">
                            : {{ $order_date }}
                        </td>
                    </tr>
                    <tr>
                        <td width="15%">
                            Ruangan
                        </td>
                        <td width="35%">
                            : {{ $order_room }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Total Linen
                        </td>
                        <td>
                            : {{ count($order_linen_line) }}
                        </td>
                    </tr>
                </table>
                <div class="gap"></div>
                <table width="100%" class="table-border">
                    <tr>
                        <th width="70px">
                            No.
                        </th>
                        <th>
                            Kode Linen
                        </th>
                        <th>
                            Nama
                        </th>
                    </tr>
                    @foreach($order_linen_line as $index => $oll)
                    <tr>
                        <td align="center" width="8px">
                            {{ $index + 1}}
                        </td>
                        <td align="center">
                            {{ $oll->linen_code }}
                        </td>
                        <td align="center">
                            {{ $oll->linen_name }}
                        </td>
                    </tr>
                    @endforeach
                </table>
                <div class="gap"></div>
                <table width=100%>
                    <tr>
                        <!-- <td width=30%>&nbsp;</td>
                        <td width=30%>&nbsp;</td> -->
                        <td width=35% align="center">Dibuat Oleh</td>
                        <td width=35% align="center">Disetujui Oleh</td>
                    </tr>
                    <tr><td colspan=2>&nbsp;</td></tr>
                    <tr><td colspan=2>&nbsp;</td></tr>
                    <tr><td colspan=2>&nbsp;</td></tr>
                    <tr>
                        <!-- <td width=30%>&nbsp;</td>
                        <td width=30%>&nbsp;</td> -->
                        <td width=35% align="center">_______________</td>
                        <td width=35% align="center">_______________</td>
                    </tr>
                </table>
            </td>
            <td>
                <table width="100%">
                    <tr>
                        <td colspan="2">
                            <h2>RS HARAPAN MAGELANG</h2>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="font-size: 16px">
                            Jl. Panembahan Senopati No. 11
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="font-size: 16px">Magelang</td>
                    </tr>
                    <tr>
                        <td colspan="2" style="font-size: 16px">Telp. 0298 364033</td>
                    </tr>
                </table>
                <div class="gap"></div>
                <hr>
                <table width="100%">
                    <tr>
                        <td align="center" style="font-size: 20px">ORDER LINEN</td>
                    </tr>
                </table>
                <hr>
                <div class="gap"></div>
                <table width="100%">
                    <tr>
                        <td width="30%">
                            Tanggal Order      
                        </td>
                        <td width="50%">
                            : {{ $order_date }}
                        </td>
                    </tr>
                    <tr>
                        <td width="15%">
                            Ruangan
                        </td>
                        <td width="35%">
                            : {{ $order_room }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Total Linen
                        </td>
                        <td>
                            : {{ count($order_linen_line) }}
                        </td>
                    </tr>
                </table>
                <div class="gap"></div>
                <table width="100%" class="table-border">
                    <tr>
                        <th width="70px">
                            No.
                        </th>
                        <th>
                            Kode Linen
                        </th>
                        <th>
                            Nama
                        </th>
                    </tr>
                    @foreach($order_linen_line as $index => $oll)
                    <tr>
                        <td align="center" width="8px">
                            {{ $index + 1}}
                        </td>
                        <td align="center">
                            {{ $oll->linen_code }}
                        </td>
                        <td align="center">
                            {{ $oll->linen_name }}
                        </td>
                    </tr>
                    @endforeach
                </table>
                <div class="gap"></div>
                <table width=100%>
                    <tr>
                        <!-- <td width=30%>&nbsp;</td>
                        <td width=30%>&nbsp;</td> -->
                        <td width=35% align="center">Dibuat Oleh</td>
                        <td width=35% align="center">Disetujui Oleh</td>
                    </tr>
                    <tr><td colspan=2>&nbsp;</td></tr>
                    <tr><td colspan=2>&nbsp;</td></tr>
                    <tr><td colspan=2>&nbsp;</td></tr>
                    <tr>
                        <!-- <td width=30%>&nbsp;</td>
                        <td width=30%>&nbsp;</td> -->
                        <td width=35% align="center">_______________</td>
                        <td width=35% align="center">_______________</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>