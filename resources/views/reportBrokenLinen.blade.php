@extends('layouts.master')

@section('style')
	@parent
	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  	<link rel="stylesheet" href="{{ asset('adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
  	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
@endsection 

@section('content')
	<section class="content-header">
		<div class="container-fluid">
			<div class="container-fluid">
				<div class="row mb-2">
					<div class="col-sm-6">
						<h1 class="m-0 text-dark">Linen Rusak</h1>
					</div>
					<div class="col-sm-6">
						<ol class="breadcrumb float-sm-right">
							<li class="breadcrumb-item"><a href="/dashboard">Beranda</a></li>
							<li class="breadcrumb-item active">Linen Rusak</li>
						</ol>
					</div>
				</div>
			</div>
		</div>
	</section>
    
    <section class="content">
    	<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<div class="card card-primary card-outline">
						<div class="card-header">
							<div class="col-6">
								<a href="/report/broken/export" data-toggle="modal" class="col-3 btn btn-block bg-gradient-warning" target="new"> Export Excel</a>	
							</div>							
						</div>
						<div class="card-body">
							<table id="broken-linen-list" class="table table-bordered table-hover" width="100%">
								<thead>
									<tr>
										<th width="20px">No</th>
										<th>Kode Linen</th>
										<th>Nama</th>
										<th>Satuan</th>
										<th>Penyebab Rusak</th>
									</tr>
	                			</thead>
								<tbody>
								</tbody>								
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<div class="modal fade" id="choose-broken-export">
        <div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Pilih Data untuk Dieksport</h4>
					<button type="button" class="close cancel-submit-line" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
            	</div>
            	<div class="modal-body">
            		{{ Form::hidden('line_id', '', ['id'=>'line-id']) }}
            		<div class="form-group row">
						<label class="col-sm-2 col-form-label">Pilih Linen</label>
						<div class="col-sm-10">
							{{ Form::select('export_format', [
								""=> " -- Pilih Format -- ",
								"group" => "Grup Linen",
								"detail" => "Detail"
								], '', ['class'=>'form-control select2', 'id'=>'export-format', 'required']) }}
						</div>
					</div>
            	</div>
            	<div class="modal-footer justify-content-between">
            		<button class="btn btn-danger cancel-submit-line" data-dismiss="modal">Batal</button>
            		<button class="btn btn-primary" id="export-broken">Export</button>
            	</div>
        	</div>
    	</div>
    </div>
@endsection 

@section('js')
	@parent 
	<script src="{{ asset('adminlte/plugins/datatables/jquery.dataTables.js') }}"></script>
	<script src="{{ asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
	<script type="text/javascript" src="{{ asset('/js/report.js') }}"></script>
@endsection