<html>
<head>
    <title>Daftar Linen yang sedang dicuci</title>
<style type="text/css">

</style>
</head>
<body>
    <table id="wash-list" class="display" border="1">
        <thead>
            <tr>
                <th>No</th>
                
                <th>Nama Linen</th>
                <th>Jumlah Digunakan</th>
                <th>Satuan</th>
            </tr>
        </thead>
        <tbody>
            @foreach($washed_linen as $wl)
            <tr>
                <td>{{ $wl->no }}</td>
                
                <td>{{ $wl->linen_name }}</td>
                <td>{{ $wl->qty }}</td>
                <td>{{ $wl->uom }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>