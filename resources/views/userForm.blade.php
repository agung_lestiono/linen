@extends('layouts.master')

@section('style')
	@parent

@endsection

@section('content')
	<section class="content-header">
		<div class="container-fluid">
			<div class="container-fluid">
				<div class="row mb-2">
					<div class="col-sm-6">
						<h1 class="m-0 text-dark">{{ $pagetitle }}</h1>
					</div>
					<div class="col-sm-6">
						<ol class="breadcrumb float-sm-right">
							<li class="breadcrumb-item"><a href="/dashboard">Beranda</a></li>
							<li class="breadcrumb-item"><a href="/linen">Linen</a></li>
							<li class="breadcrumb-item active">{{$pagetitle}}</li>
						</ol>
					</div>
				</div>
			</div>
		</div>
	</section>
    
    <section class="content">
    	<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<div class="card card-primary card-outline">
						<div class="card-body">
							<form method="POST" action="/user/store">
								@csrf
								<input type="hidden" id="id" name="id" value="{{ $id }}">
								<div class="form-group row">
                    				<label class="col-sm-2 col-form-label">Nama</label>
                    				<div class="col-sm-10">
                      					{{ Form::text('name', $name, ['class'=>'form-control', 'id'=>'name', 'required']) }}
                    				</div>
                    			</div>

                    			<div class="form-group row">
                    				<label class="col-sm-2 col-form-label">Username</label>
                    				<div class="col-sm-10">
                    					{{ Form::text('username', $username, ['class'=>'form-control','id'=>'username', 'required']) }}
                    				</div>
                    			</div>

                    			<div class="form-group row">
                    				<label class="col-sm-2 col-form-label">Email</label>
                    				<div class="col-sm-10">
                    					{{ Form::text('email', $email, ['class'=>'form-control','id'=>'email', 'required']) }}
                    				</div>
                    			</div>

                    			<div class="form-group row">
                    				<label class="col-sm-2 col-form-label">Posisi</label>
                    				<div class="col-sm-10">
                      					<div class="form-group">
						                  	{{ Form::select('role', ['superadmin'=>'Super Administrator', 'admin_linen'=>'Admin Linen', 'admin_room'=>'Admin Ruangan'],$role, ['class'=>'form-control select2', 'id'=>'role', 'required']) }}
						                </div>
                    				</div>
                  				</div>

                  				<div class="form-group row">
                    				<label class="col-sm-2 col-form-label">Password</label>
                    				<div class="col-sm-10">
                    					{{ Form::password('password', ['class'=>'form-control', 'id'=>'password']) }}
                    				</div>
                  				</div>

                  				<div class="form-group row">
	                  				<button type="submit" class="btn btn-block bg-gradient-primary col-1">Simpan</button>
	                  				
                  				</div>
							</form>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection

@section('js')
	@parent
	<script src="{{ asset('js/user.js') }}"></script>
@endsection