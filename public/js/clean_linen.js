$(document).ready(function () {
	if ($('#clean-linen-list').length) {
		var clean_linen_list = $('#clean-linen-list').DataTable({
			'ajax' : {
				'url' : 'cleanlinen/list',
				dataSrc : ''
			},
			'columnsDef' : [
				
			],
			'columns' : [
				{data:'no'},
				{data:'order_no'},
				{data:'linen_code'},
				{data:'linen_name'},
				{data:'qty'},
				{data:'action'}
			]
		})
	}

	$('#clean-linen-list').on('click', '.clean-linen-btn', function(e) {
		e.preventDefault();   
		var order_id = $(this).data('order-id');
		var linen_line_id = $(this).data('linen-line-id');
		var linen_code = $(this).data('linen-code');
		var linen_name = $(this).data('linen-name');
		
		$('#modal-use-linen-order-id').val(order_id)
		$('#modal-linen-line-id').val(linen_line_id);
		$('#modal-linen-code').val(linen_code);
		$('#modal-linen-name').val(linen_name);
	})

	$('#clean-linen-list').on('click', '.clean-order-linen', function(e) {
		e.preventDefault();
		var order_id = $(this).data('modal-order-id');
		var order_no = $(this).data('modal-order-no');
		$('#modal-order-id').val(order_id);
		$('#modal-order-no').text(order_no);
		$('.linen-item').remove();

		$.get('/cleanorder/' + order_id, function (data) {
			data.linen_item.forEach(function (linen_item) {
				$('#ready-clean-linen-table').append(
					'' +
                    '<tr class="linen-item">' +
                    '<td>' + '<span class="line-id" style="display:none">' + linen_item.id + '</span> <span data-name="' + linen_item.linen_code + '" class="linen-code">' +
                    linen_item.linen_code + '</span> ' + '</td>' +
                    '<td>' + '<span data-name="' + linen_item.linen_name + '" class="linen-name">' +
                    linen_item.linen_name + '</span> ' + '</td>' +
                    '<td><input type="checkbox" name="check[]" value=1 class="item-check" value="' + linen_item.id + '" checked></td>' +
                    '</tr>'
                );
			})
		})
	})

	$('.cancel-clean-order').click(function() {
		$('#clean-order-modal').modal('hide');
	})

	$('#form-restock-order').submit(function(e) {
		show_loading();
		e.preventDefault();

		$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var linen_items = [];

        $('.linen-item').each(function () {
        	var chk = $(this).find('input:checkbox');
            linen_items.push($(this).find('.line-id').text() + "|" + $(this).find('.linen-code').text() + "|" + $(this).find('.linen-name').text()  + "|" + chk[0].checked);
        });

        $.post($(this).attr('action'), {
        	order_id : $('#modal-order-id').val(),
        	linen_items : linen_items
        }).done(function(msg) {
        	if(msg.status == 'success')
        		window.location.href = '/cleanlinen';
        	else
        		alert('Gagal memproses data. Silakan ulangi lagi atau hubungi administrator.');
        })
	})

})