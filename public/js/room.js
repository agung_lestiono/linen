$(document).ready(function () {
	if($('.form-control').hasClass('select2'))
		$('.select2').select2();

	if ($('#room-list').length) {
		var location_list = $('#room-list').DataTable({
			'ajax' : {
				'url' : 'room/list',
				dataSrc : ''
			},
			'columnDefs' : [
				{ className: "text-center", "targets":[0,3] }
			],
			'columns' : [
				{data:'no'},
				{data:'name'},
				{data:'officer'},
				{data:'action'}
			]
		})
	}

	if ($('#edit-mode').val() == 0) {
		$('#div-cancel-btn').hide();
		$('#name').prop('readonly', true);
		$('select').prop('disabled', true);
		$('#submit-room-btn').hide();
	}

	$('#do-edit-btn').click(function () {
		$('#div-cancel-btn').show();
		$('#name').prop('readonly', false);
		$('select').prop('disabled', false);
		$('#submit-room-btn').show();
		$('#do-edit-btn').hide();
	})

	$('#cancel-edit-btn').click(function () {
		$('#div-cancel-btn').hide();
		$('#name').prop('readonly', true);
		$('select').prop('disabled', true);
		$('#submit-room-btn').hide();
		$('#do-edit-btn').show();
	})
})