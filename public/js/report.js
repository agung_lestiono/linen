$(document).ready(function() {
	if($('.form-control').hasClass('select2'))
		$('.select2').select2();

	if ($('#stock-list').length) {
		var stock_list = $('#stock-list').DataTable({
			'ajax' : {
				'url' : '/report/getstock',
				dataSrc : ''
			},
			'columnsDef' : [
				
			],
			'columns' : [
				{data:'no'},
				{data:'linen_name'},
				{data:'stock_qty'},
				{data:'uom'},
			]
		})
	}

	$('#export-stock').click(function() {
		show_loading();
		if ($('#template-stock option:selected').val() == 'group')
			window.location = "/report/stock/export/group";
		else
			window.location = "/report/stock/export/detail";
		
		$('#choose-template-stock').modal('toggle');
		hide_loading();
	})

	if ($('#used-linen-list').length) {
		var stock_list = $('#used-linen-list').DataTable({
			'ajax' : {
				'url' : '/report/getuselinen',
				dataSrc : ''
			},
			'columnsDef' : [
				
			],
			'columns' : [
				{data:'no'},
				{data:'linen_name'},
				{data:'qty'},
				{data:'uom'},
			]
		})
	}

	if ($('#washed-linen-list').length) {
		var stock_list = $('#washed-linen-list').DataTable({
			'ajax' : {
				'url' : '/report/getwashlinen',
				dataSrc : ''
			},
			'columnsDef' : [
				
			],
			'columns' : [
				{data:'no'},
				{data:'linen_name'},
				{data:'qty'},
				{data:'uom'},
			]
		})
	}

	if ($('#broken-linen-list').length) {
		var broken_list = $('#broken-linen-list').DataTable({
			'ajax' : {
				'url' : '/report/getbroken',
				dataSrc : ''
			},
			'columnsDef' : [
				
			],
			'columns' : [
				{data:'no'},
				{data:'linen_code'},
				{data:'linen_name'},
				{data:'uom'},
				{data:'broken_reason'},
			]
		})
	}
})

	