$(document).ready(function () {
	if($('.form-control').hasClass('select2'))
		$('.select2').select2();

	if ($('#linen-list').length) {
		var linen_list = $('#linen-list').DataTable({
			'ajax' : {
				'url' : 'linen/list',
				dataSrc : ''
			},
			'columnDefs' : [
				{className:"text-center", "targets":[0,3,5]},
				{className:"text-right", "targets":[4]}
			],
			'columns' : [
				{data:'no'},
				{data:'linen_code'},
				{data:'linen_name'},
				{data:'uom'},
				{data:'frequencies_number'},
				{data:'action'}
			]
		})
	}

	if ($('#edit-mode').val() == 0) {
		$('#div-cancel-btn').hide();
		$('#linen-code').prop('readonly', true);
		// $('#linen-name').prop('readonly', true);
		$('select').prop('disabled', true);
		$('#procurement-date').prop('disabled', true);		
		$('#frequencies-number').prop('disabled', true);
		$('#active').prop('disabled', true);
		$('#broken').prop('disabled', true);
		$('#broken-reason').prop('readonly', true);
		$('#submit-linen-btn').hide();
	}

	$('#do-edit-btn').click(function () {
		$('#div-cancel-btn').show();
		$('#linen-code').prop('readonly', false);
		// $('#linen-name').prop('readonly', false);
		$('select').prop('disabled', false);
		$('#procurement-date').prop('disabled', false);
		$('#active').prop('disabled', false);
		$('#broken').prop('disabled', false);
		if($('#broken:checked').length){
			$('#broken-reason').prop('readonly',false); 
	    }
		$('#submit-linen-btn').show();
		$('#do-edit-btn').hide()
	})

	$('#cancel-edit-btn').click(function () {
		$('#div-cancel-btn').hide();
		$('#linen-code').prop('readonly', true);
		// $('#linen-name').prop('readonly', true);
		$('select').prop('disabled', true);
		$('#procurement-date').prop('disabled', true);
		$('#active').prop('disabled', true);
		$('#broken').prop('disabled', true);
		$('#broken-reason').prop('readonly', true);
		$('#submit-linen-btn').hide();
		$('#do-edit-btn').show()
	})

	

	$('#broken').change(function(){
        if($('#broken:checked').length){
            $('#broken-reason').prop('readonly',false); 
        }else{
            $('#broken-reason').prop('readonly',true);
            $('#broken-reason').val('');
        }
    });

	$('.datepicker').datepicker({
		format: 'dd-mm-yyyy',
		autoclose : true
	});

	$("#group-id").change(function () {
        if($("#group-id option:selected").val()){
            $.get("/linen/getgroup/" + $("#group-id option:selected").val(), function (data) {
                $('#linen-name').val(data.group_name);
            });
        }
    });
	
})