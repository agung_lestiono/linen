$(document).ready(function () {
	if ($('#use-order-list').length) {
		var use_order_list = $('#use-order-list').DataTable({
			'ajax' : {
				'url' : 'useorder/list',
				dataSrc : ''
			},
			'columnsDefs' : [
				{ ClassName: "text-center", "targets":[0,3] }
			],
			'columns' : [
				{data:'no'},
				{data:'order_no'},
				{data:'order_date'},
				
				{data:'action'}
			]
		})
	}

	$('#use-order-list').on('click', '.use-order-btn', function(e) {
		e.preventDefault();   
		
	})


})