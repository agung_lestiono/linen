$(document).ready(function() {

	if($('.form-control').hasClass('select2'))
		$('.select2').select2();

    $('.datepicker').datepicker({
        format: 'dd-mm-yyyy',
        autoclose : true
    });

	if ($('#order-list').length) {
		var order_list = $('#order-list').DataTable({
			'ajax' : {
				'url' : '/orderlinen/list',
				dataSrc : '',
			},
			'columnDefs' : [
                {className:"text-center", "targets":[0,1,2,6]}
			],
			'columns' : [
				{data:'no'},
				{data:'order_no'},
				{data:'order_date'},
				{data:'room'},
				{data:'ordered_by'},
				{data:'order_status'},
				{data:'action'}
			] 
		})
	}

    if ($('#edit-mode').val() == 0) {
        $('#div-cancel-btn').hide();
        $('#order-date').prop('disabled', true);
        $('select').prop('disabled', true);
        $('#add-linen-btn').hide();
        $('#order-submit-btn').hide();
        $('.print-order').show();
    }

    $('#do-edit-btn').click(function () {
        $('#order-date').prop('disabled', false);
        $('select').prop('disabled', false);
        $('#add-linen-btn').show();
        $('#order-submit-btn').show();
        $('#do-edit-btn').hide();
        $('#approve-btn').hide();
        $('#div-cancel-btn').show();
        $('.action-col').show();
        $('.print-order').hide();
    })

    $('#cancel-edit-btn').click(function () {
        $('#order-date').prop('disabled', true);
        $('select').prop('disabled', true);
        $('#add-linen-btn').hide();
        $('#order-submit-btn').hide();
        $('#do-edit-btn').show();
        $('#approve-btn').show();
        $('#div-cancel-btn').hide();
        $('.action-col').hide();
        $('.print-order').show();
    })

    if ($('#id').val() != '' || $('#id').val() != 0) {
        $.get('/orderlinen/getline/' + $('#id').val(), function (data) {
            data.forEach(function (line) {
                $('#order-linen-line').append('' + 
                '<tr class="line-item">' +
                    '<td>' + 
                        '<span class="line-id" style="display: none">' + line.id + '</span>' +
                        '<span class="linen-id" style="display: none">' + line.linen_id + '</span>' +
                        '<span class="linen-code">' + line.linen_code + '</span> ' +
                    '</td>' +
                    '<td>' + 
                        '<span class="linen-name">' + line.linen_name + '</span> ' +
                    '</td>' +
                    '<td>' + 
                        '<span class="linen-qty" style="text-align: right;">' + 1 + '</span> ' +
                    '</td>' +
                    '<td td align="center" style="text-align: center; width: 100px" class="action-col">' +
                        line.action +
                    '</td>' +
                '</tr>');
            })

            
            $('.action-col').hide();
        })
    }

	$("#linen-id").change(function () {
        if($("#linen-id option:selected").val()){
            $.get("/orderlinenline/getlinen/" + $("#linen-id option:selected").val(), function (data) {
                $('#linen-code').val(data.linen.linen_code);
                $('#linen-name').val(data.linen.linen_name);
            });
        }
    }); 
  	
  	$('#submit-line').click(function (e) {
        e.preventDefault();

        var order_linen_line_ids = [];

        $.each($('.line-item'), function (){
            order_linen_line_ids.push($(this).find('.linen-id').text());
        })
        var check_linen_id = $.inArray($("#linen-id option:selected").val(), order_linen_line_ids);

        if (check_linen_id>=0) {
            alert('Error! Linen tidak boleh sama!');
        }
        else {
            $('#order-linen-line').append('' + 
                '<tr class="line-item">' +
                    '<td>' + 
                        '<span class="line-id" style="display: none;">0</span>' +
                        '<span class="linen-id" style="display: none;">' + $("#linen-id option:selected").val() + '</span>' + 
                        '<span class="linen-code">' + $('#linen-code').val() + '</span> ' +
                    '</td>' +
                    '<td>' + 
                        '<span class="linen-name">' + $('#linen-name').val() + '</span> ' +
                    '</td>' +
                    '<td>' + 
                        '<span class="linen-qty" style="text-align: right;">' + 1 + '</span> ' +
                    '</td>' +
                    '<td align="center">' +        
                        '<a href = "#" class= "delete-line text-danger" style="text-align : right"> <i class="far fa-trash-alt"></i></a >' +
                    '</td>' +
                '</tr>');

            $('#add-linen-modal').modal('toggle');
            $("#linen-id").select2('val', ' ');
            $('#linen-code').val('');
            $('#linen-name').val('');
            $('#linen-qty').val('');
        }
        
    });

    $('.cancel-submit-line').click(function() {
        $("#linen-id").select2('val', ' ');
        $('#linen-code').val('');
        $('#linen-name').val('');
        $('#linen-qty').val('');
    })

    $(document).on('click', ".delete-line", function(e) {
    // $('#order-line-body').on('click', '.delete-line', function (e) {
    	e.preventDefault();
        $(this).closest('tr').remove();
    })

    $('#form-order-linen').submit(function (e) {
        show_loading();
        e.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var order_line = [];
        var unsave_qty = [];
        $.each($('.line-item'), function (){
            if ($(this).find('.linen-qty').text() == '') {
                unsave_qty.push(1);
            }
            order_line.push($(this).find('.line-id').text() + "|" + $(this).find('.linen-id').text() + "|" + $(this).find('.linen-code').text() + "|" + $(this).find('.linen-name').text());
        })

        if (order_line.length > 0) {
            $.post($('#form-order-linen').attr('action'), {
                id : $('#id').val(),
                order_date : $('#order-date').val(),
                room_id : $('#room-id').val(),
                line_data : order_line,
            }).done(function (result){
                var new_id = result.inserted_id;
                window.location.href = '/orderlinen/' + new_id;
            })
        }
        else {
            alert('Error, Detail Order tidak boleh kosong!');
        }
    })

    $('#approve-btn').click(function (e) {
        show_loading();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var confirm_approve = confirm('Anda yakin ingin menyetujui order ini?');
        if (confirm_approve == true) {
            e.preventDefault();
            $.post('/orderlinen/approve', {
                id : $('#id').val(),
            }).done(function (result) {
                order_id = $('#id').val(),
                window.location.href = '/orderlinen/' + order_id
            })
        }
        
    })

    $('.print-order').on('click', function (e) {
        var printWindow = window.open('/print/orderlinen/' + $('#id').val());
        printWindow.onload = function () {
            printWindow.print();
            printWindow.onfocus = function () {
                setTimeout(function () {
                    printWindow.close();
                }, 500);
            }
        }
    });
})
