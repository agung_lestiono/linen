$(document).ready(function () {

	if ($('#group-list').length) {
		group_list = $('#group-list').DataTable({
			'ajax' : {
				'url' : 'grouplinen/list',
				dataSrc : ''
			},
			'columnDefs' : [
				{className:"text-center", "targets":[0,3]}
			],
			'columns' : [
				{data:'no'},
				{data: 'group_code'},
				{data:'group_name'},
				{data:'action'}
			]
		})
	}
	
	if ($('#edit-mode').val() == 0) {
		$('#div-cancel-btn').hide();
		$('#group-code').prop('readonly', true);
		$('#group-name').prop('readonly', true);
		$('#submit-grouplinen-btn').hide();
	}

	$('#do-edit-btn').click(function () {
		$('#div-cancel-btn').show();
		$('#group-code').prop('readonly', false);
		$('#group-name').prop('readonly', false);
		$('#submit-grouplinen-btn').show();
		$('#do-edit-btn').hide();
	})

	$('#cancel-edit-btn').click(function () {
		$('#div-cancel-btn').hide();
		$('#group-code').prop('readonly', true);
		$('#group-name').prop('readonly', true);
		$('#submit-grouplinen-btn').hide();
		$('#do-edit-btn').show();
	})

	$('#form-use-order').submit(function() {
		show_loading();
	})
})