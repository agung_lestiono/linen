$(document).ready(function () {
	if($('.form-control').hasClass('select2'))
		$('.select2').select2();

	if ($('#user-list').length) {
		var user_list = $('#user-list').DataTable({
			'ajax' : {
				'url' : 'user/list',
				dataSrc : ''
			},
			'columnDefs' : [
				{className:"text-center", "targets":[0,4]},
			],
			'columns' : [
				{data:'no'},
				{data:'name'},
				{data:'username'},
				{data:'role'},
				{data:'action'}
			]
		})
	}
	
})