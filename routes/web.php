<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	if(!Auth::user())
    	return view('auth.login');
    else
    	return redirect('/dashboard');
});

Auth::routes();

//dashboard
Route::get('/dashboard', 'DashboardController@index')->middleware('auth');
Route::get('/dashboard/getdataorder', 'DashboardController@getDataOrder')->middleware('auth');
Route::get('/dashboard/getdatalinen', 'DashboardController@getDataLinen')->middleware('auth');

//linens
Route::get('/linen', 'LinenController@index')->middleware('auth');
Route::get('/linen/list', 'LinenController@getList')->middleware('auth');
Route::get('/linen/create', 'LinenController@create')->middleware('auth');
Route::get('/linen/{id}', 'LinenController@detail')->middleware('auth');
Route::post('/linen/store', 'LinenController@store')->middleware('auth');
Route::get('/linen/delete/{id}', 'LinenController@delete')->middleware('auth');
Route::post('/update/qty', 'LinenController@updateQty')->middleware('auth');
Route::get('/linen/getgroup/{id}', 'LinenController@getGroupData')->middleware('auth');

//grouplinen
Route::get('/grouplinen', 'GroupLinenController@index')->middleware('auth');
Route::get('/grouplinen/list', 'GroupLinenController@getList')->middleware('auth');
Route::get('/grouplinen/create', 'GroupLinenController@create')->middleware('auth');
Route::get('/grouplinen/{id}', 'GroupLinenController@detail')->middleware('auth');
Route::post('/grouplinen/store', 'GroupLinenController@store')->middleware('auth');
Route::get('/grouplinen/delete/{id}', 'GroupLinenController@delete')->middleware('auth');

//rooms
Route::get('/room', 'RoomController@index')->middleware('auth');
Route::get('/room/list', 'RoomController@getList')->middleware('auth');
Route::get('/room/create', 'RoomController@create')->middleware('auth');
Route::get('/room/{id}', 'RoomController@detail')->middleware('auth');
Route::post('/room/store', 'RoomController@store')->middleware('auth');
Route::get('/room/delete/{id}', 'RoomController@delete')->middleware('auth');

//order linen
Route::get('/orderlinen', 'OrderLinenController@index')->middleware('auth');
Route::get('/orderlinen/list', 'OrderLinenController@getList')->middleware('auth');
Route::get('/orderlinen/create', 'OrderLinenController@createOrder')->middleware('auth');
Route::get('/orderlinen/{id}', 'OrderLinenController@detailOrder')->middleware('auth');
Route::get('/orderlinen/getline/{id}', 'OrderLinenController@getOrderLine')->middleware('auth');
Route::post('/orderlinen/store', 'OrderLinenController@storeOrder')->middleware('auth');
Route::post('/orderlinen/approve', 'OrderLinenController@approveOrder')->middleware('auth');
Route::get('/orderlinen/delete/{id}', 'OrderLinenController@delete')->middleware('auth');
Route::get('/orderlinenline/getlinen/{linen_id}', 'OrderLinenController@getLinen')->middleware('auth');
Route::get('/orderlinenline/delete/{id}', 'OrderLinenController@deleteOrderLinenLine')->middleware('auth');
Route::get('/print/orderlinen/{id}', 'OrderLinenController@printOrderLinen')->middleware('auth');

//use linen
Route::get('/uselinen', 'UseLinenController@index')->middleware('auth');
Route::get('/uselinen/list', 'UseLinenController@getList')->middleware('auth');
Route::post('/uselinen/use', 'UseLinenController@useLinen')->middleware('auth');
Route::get('/useorder/{order_id}', 'UseLinenController@useOrderLinen')->middleware('auth');
Route::post('/useorder/use', 'UseLinenController@useOrderSubmit')->middleware('auth');

//use order
// Route::get('/useorder', 'UseOrderController@index')->middleware('auth');
// Route::get('/useorder/list', 'UseOrderController@getList')->middleware('auth');
// Route::get('/useorder/use/{id}', 'UseOrderController@useOrder')->middleware('auth');

//wash linen
Route::get('/washlinen', 'WashLinenController@index')->middleware('auth');
Route::get('/washlinen/list', 'WashLinenController@getList')->middleware('auth');
Route::post('/washlinen/wash', 'WashLinenController@washLinen')->middleware('auth');
Route::get('/washorder/{order_id}', 'WashLinenController@washOrderLinen')->middleware('auth');
Route::post('/washorder/wash', 'WashLinenController@washOrderSubmit')->middleware('auth');

//clean linen
Route::get('/cleanlinen', 'CleanLinenController@index')->middleware('auth');
Route::get('/cleanlinen/list', 'CleanLinenController@getList')->middleware('auth');
Route::post('/cleanlinen/restock', 'CleanLinenController@restock')->middleware('auth');
Route::get('/cleanorder/{order_id}', 'CleanLinenController@cleanOrderLinen')->middleware('auth');
Route::post('/cleanorder/restock', 'CleanLinenController@cleanOrderSubmit')->middleware('auth');

//report linen
Route::get('/report/stock', 'ReportController@stock')->middleware('auth');
Route::get('/report/getstock', 'ReportController@getStock')->middleware('auth');
Route::get('/report/stock/export/group', 'ReportController@exportStock')->middleware('auth');
Route::get('/report/stock/export/detail', 'ReportController@exportStockDetail')->middleware('auth');
Route::get('/report/broken', 'ReportController@broken')->middleware('auth');
Route::get('/report/getbroken', 'ReportController@getBroken')->middleware('auth');
// Route::get('/report/broken/export/{format}', 'ReportController@exportBroken')->middleware('auth');
Route::get('/report/broken/export', 'ReportController@exportBroken')->middleware('auth');
Route::get('/report/uselinen', 'ReportController@useLinen')->middleware('auth');
Route::get('/report/getuselinen', 'ReportController@getUseLinen')->middleware('auth');
Route::get('/report/use/export', 'ReportController@exportUse')->middleware('auth');
Route::get('/report/washlinen', 'ReportController@washlinen')->middleware('auth');
Route::get('/report/getwashlinen', 'ReportController@getWashLinen')->middleware('auth');
Route::get('/report/getwashlinen/export', 'ReportController@exportWash')->middleware('auth');
Route::get('/report/wash/export', 'ReportController@exportWash')->middleware('auth');

//user
Route::get('/user', 'UserController@index')->middleware('auth');
Route::get('/user/list', 'UserController@getList')->middleware('auth');
Route::get('/user/create', 'UserController@create')->middleware('auth');
Route::get('/user/{id}', 'UserController@detail')->middleware('auth');
Route::post('/user/store', 'UserController@store')->middleware('auth');
Route::get('/user/delete/{id}', 'UserController@delete')->middleware('auth');

Route::get('/home', 'HomeController@index')->name('home');
