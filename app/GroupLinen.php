<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupLinen extends Model
{
    protected $table = 'group_linen';
}
