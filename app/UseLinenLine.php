<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UseLinenLine extends Model
{
    protected $table = 'use_linen_line';
}
