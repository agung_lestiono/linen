<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\OrderLinen;
use App\OrderLinenLine;
use App\UseLinen;
use App\Rooms;
use App\Linens;

use PDF;

class OrderLinenController extends Controller
{
    public function index()
    {
    	return view('orderLinenList');
    }

    public function getList()
    {
    	$orders = OrderLinen::orderBy('order_date', 'desc')->get();

    	foreach ($orders as $key => $order) 
    	{
            $room = Rooms::find($order->room_id);
            if ($order->status == 'draft') {
                $status = 'Draft';
            }
            elseif ($order->status == 'approved') {
                $status = 'Disetujui';
            }
            elseif ($order->status == 'in_use') {
                $status = 'Sedang Digunakan';
            }
            elseif ($order->status == 'being_washed') {
                $status = 'Sedang Dicuci';
            }
            elseif ($order->status == 'done') {
                $status = 'Selesai';
            }
            else {
                $status = '';
            }

    		$orders[$key]->no = $key + 1;
            $orders[$key]->order_date = date('d-m-Y', strtotime($order->order_date));
            $orders[$key]->order_status = $status;
            $orders[$key]->room = ($room) ? $room->name : '';
    		$orders[$key]->action = '<a href="/orderlinen/' . $order->id . '" class="btn btn-info"> <i class="fas fa-info-circle"></i></a>';
            if ($order->status == 'draft') {
                if (Auth::user()->role == 'admin_room' || Auth::user()->role == 'superadmin') {
                    $orders[$key]->action .= '<a href="/orderlinen/delete/' . $order->id . '" class="btn btn-danger" onclick="if(!confirm(\'Anda yakin ingin menghapus order ini?\')) return false;"><i class="far fa-trash-alt"></i></a>';
                }
            }
    	}

    	return response()->json($orders);
    }

    public function createOrder()
    {
        $room_list = array('' => ' -- Pilih Ruangan --', );
        $rooms = Rooms::all();
        foreach ($rooms as $key => $r) {
            $room_list[$r->id] = $r->name;
        }

        $linen_list = array('' => ' -- Pilih Linen --', );
        $linens = Linens::where('in_use', '=', 0)
                            ->where('active', 1)
                            ->orWhere('broken', 0)
                            ->get();
        foreach ($linens as $key => $l) {
            $linen_list[$l->id] = $l->linen_code . ' - ' . $l->linen_name;
        }

    	return view('orderLinenForm', [
    		'pagetitle'		=> 'Tambah Order',
            'edit_mode'     => 1,
    		'id' 			=> '',
    		'order_no' 		=> '',
    		'order_date' 	=> date('d-m-Y'),
    		'room_list'     => $room_list,
            'room_id'       => '',
    		'ordered_by' 	=> '',
    		'approved_by'	=> '',
    		'status'		=> '',
            'linen_list'    => $linen_list,
            'order_line'    => '',
            'total_linen'   => 0,
    	]);
    }

    public function detailOrder($id)
    {
        $room_list = array('' => ' -- Pilih Ruangan -- ', );
        $rooms = Rooms::all();
        foreach ($rooms as $key => $r) {
            $room_list[$r->id] = $r->name;
        }

        $linen_list = array('' => ' -- Pilih Linen --', );
        $linens = Linens::where('in_use', '!=', 1)
                            ->where('active', 1)
                            ->orWhere('broken', 0)
                            ->get();
        foreach ($linens as $key => $l) {
            $linen_list[$l->id] = $l->linen_code . ' - ' . $l->linen_name;
        }

        $order = OrderLinen::find($id);
        
        $order_line = OrderLinenLine::where('order_linen_id', $order->id)->get();
        foreach ($order_line as $key => $ol) {
            if ($order->status == 'draft') {
                $order_line[$key]->action .= '<a href = "#" class= "delete-line text-danger"> <i class="far fa-trash-alt"></i></a >';
            }
        }

        $status = $order->status;

        if ($status == 'draft'){
            $new_status = 'Draft';
        }
        elseif ($status == 'approved') {
            $new_status = 'Disetujui';
        }
        elseif ($status == 'in_use') {
            $new_status = 'Sedang Digunakan';
        }
        elseif ($status == 'being_washed') {
            $new_status = 'Sedang Dicuci';
        }
        elseif ($status == 'done') {
            $new_status = 'Selesai';
        }
        else {
            $new_status = '';
        }

        $total_linen = OrderLinenLine::where('order_linen_id', $id)->count('id');

        return view('orderLinenForm', [
            'pagetitle'     => 'Order Linen ' . $order->order_no,
            'edit_mode'     => 0,
            'id'            => $order->id,
            'order_no'      => $order->order_no,
            'order_date'    => date('d-m-Y', strtotime($order->order_date)),
            'room_list'     => $room_list,
            'room_id'       => $order->room_id,
            'ordered_by'    => $order->ordered_by,
            'approved_by'   => $order->approved_by,
            'status'        => $order->status,
            'status_string' => $new_status,
            'linen_list'    => $linen_list,
            'order_line'    => $order_line,
            'approved_by'   => $order->approved_by,
            'total_linen'    => $total_linen,
        ]);
    }

    public function storeOrder(Request $request)
    {
        $order_id = $request->id;
        if ($order_id)
        {
            $order = OrderLinen::find($order_id);
            $order->updated_by = Auth::user()->username;
            $order->order_date = date('Y-m-d', strtotime($request->order_date));
            $order->room_id = $request->room_id;
            $order->created_by = Auth::user()->username;
            $order->updated_by = Auth::user()->username;
            $order->status = 'draft';
            $order->save();
            
            $ids_before = [];
            $ids_after = [];
            
            $line_before = OrderLinenLine::where('order_linen_id', $order_id)->get();
            foreach ($line_before as $key => $lb) {
                array_push($ids_before, $lb->id);
            }

            $order_line = $request->line_data;
            foreach ($order_line as $key => $ol) {
                $row_ol = explode('|', $ol);
                $line_id = $row_ol[0];
                if ($line_id > 0) {
                    $new_ol = OrderLinenLine::find($line_id);
                    $linen = Linens::find($row_ol[1]);
                    $linen->save();
                }
                else {
                    $new_ol = new OrderLinenLine;
                }
                
                $new_ol->order_linen_id = $order->id;
                $linen = Linens::find($row_ol[1]);
                $linen->in_use = 1;
                $linen->save();
                $new_ol->linen_id = $linen->id;
                $new_ol->linen_code = $row_ol[2];
                $new_ol->linen_name = $row_ol[3];
                $new_ol->linen_qty = 1;
                $new_ol->save();

                array_push($ids_after, $new_ol->id);
            }

            if (count($ids_before) > count($ids_after)){
                $ids_diff = array_diff($ids_before, $ids_after);
                if ($ids_diff) {
                    foreach ($ids_diff as $key => $idiff) {
                        $delete_line = OrderLinenLine::find($idiff);
                        $back_linen = Linens::find($delete_line->linen_id);
                        $back_linen->in_use = 0;
                        $back_linen->save();
                        $delete_line->delete();
                    }
                }
            }
        }
        else
        {
            $order = new OrderLinen;
            $last_order = OrderLinen::orderBy('id', 'desc')->first();
            if($last_order) {
                $last_no = substr($last_order->order_no, 2);
                $order->order_no = 'OL' . str_pad($last_no + 1, 6, "0", STR_PAD_LEFT);
            }
            else {
                $order->order_no = 'OL000001';
            }
            
            $order->order_date = date('Y-m-d', strtotime($request->order_date));
            $order->room_id = $request->room_id;
            $order->ordered_by = Auth::user()->username;
            $order->created_by = Auth::user()->username;
            $order->updated_by = Auth::user()->username;
            $order->status = 'draft';
            $order->save();
            
            $order_line = $request->line_data;
            foreach ($order_line as $key => $ol) {
                $new_ol = new OrderLinenLine;
                $row_ol = explode('|', $ol);
                $new_ol->order_linen_id = $order->id;
                $linen = Linens::find($row_ol[1]);
                $linen->in_use = 1;
                $linen->qty = 0;
                $linen->save();
                $new_ol->linen_id = $linen->id;
                $new_ol->linen_code = $row_ol[2];
                $new_ol->linen_name = $row_ol[3];
                $new_ol->save();
            }
        }

        $data['inserted_id'] = $order->id;

        return response()->json($data);
    }

    public function approveOrder(Request $request)
    {
        $id = $request->id;
        OrderLinen::where('id',$id)->update([
            'status' => 'approved',
            'approved_by' => Auth::user()->username,
            'updated_by' => Auth::user()->username
        ]);

        $order_linen_line = OrderLinenLine::where('order_linen_id', $id)->get();

        foreach ($order_linen_line as $key => $oll) {
            $new_use_linen = new UseLinen;
            $new_use_linen->order_id = $oll->order_linen_id;
            $new_use_linen->linen_line_id = $oll->id;
            $new_use_linen->linen_code = $oll->linen_code;
            $new_use_linen->linen_name = $oll->linen_name;
            $new_use_linen->qty = 1;
            $new_use_linen->status = 'ready_to_use';
            $new_use_linen->created_by = Auth::user()->username;
            $new_use_linen->save();
        }
    }

    public function delete($id)
    {
        $order_linen = OrderLinen::find($id);
        $order_linen->delete();
        $order_linen_line = OrderLinenLine::where('order_linen_id', $id)->get();

        foreach ($order_linen_line as $key => $od) {
            $linen = Linens::find($od->linen_id);
            $linen->qty += $od->linen_qty;
            $linen->save();
        }

        return redirect('/orderlinen')->with('message', '<div class="alert alert-warning alert-dismissible"> Order Linen berhasil dihapus.</div>');
    }

    public function getLinenList($order_id)
    {
    	$order_items = OrderLinenLine::where('order_linen_id', $order_id)->get();
    }

    public function getLinen($linen_id)
    {
        $linen = Linens::find($linen_id);
        $data['linen'] = $linen;
        return response()->json($data);
    }

    public function printOrderLinen($id)
    {
        $order_linen = OrderLinen::find($id);
        $order_linen_line = OrderLinenLine::where('order_linen_id', $id)->get();
        $room = Rooms::find($order_linen->room_id);

        $pdf = PDF::loadView('orderPrint', [
            'order_no' => $order_linen->order_no,
            'order_date' => date('d F Y', strtotime($order_linen->order_date)),
            'order_room' => $room->name,
            'order_linen_line' => $order_linen_line
        ]);

        $pdf->setOption('user-style-sheet',__DIR__.'/../../../public/css/print.css')
            ->setPaper('letter')
            ->setOption('orientation', 'portrait')
            ->setOption('margin-top', 5)
            ->setOption('margin-bottom', 5)
            ->setOption('margin-right', 5)
            ->setOption('margin-left', 5);
        return $pdf->inline('order'. $order_linen->order_no .'.pdf');
    }

    public function getOrderLine($id)
    {
        $order = OrderLinen::find($id);
        $order_line = OrderLinenLine::where('order_linen_id', $id)->get();
        foreach ($order_line as $key => $ol) {
            if ($order->status == 'draft') {
                $order_line[$key]->action .= '<a href = "#" class= "delete-line text-danger"> <i class="far fa-trash-alt"></i></a >';
            }
        }

        return response()->json($order_line);
    }
}
