<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\OrderLinen;
use App\OrderLinenLine;
use App\UseLinen;
use App\WashLinen;
use App\Linens;


class UseLinenController extends Controller
{
    public function index()
    {
    	return view('useLinenList');
    }

    public function getList()
    {
    	$linen_items = UseLinen::select('linen_line_id', 'order_id','linen_code', 'linen_name')
                                ->selectRaw('sum(qty) as qty')
                                ->groupBy('linen_line_id','order_id','linen_line_id', 'linen_code', 'linen_name')
                                ->havingRaw('sum(qty)>0')
    							->get();

    	foreach ($linen_items as $key => $li) {
            $order = OrderLinen::find($li->order_id);
    		$linen_items[$key]->no = $key + 1;
            $linen_items[$key]->order_no = '<a href="#use-order-modal" data-toggle="modal" data-modal-order-id="' . $li->order_id . '" data-modal-order-no="' . $order->order_no. '" class="use-order-linen">' . $order->order_no . '</a>';
    		$linen_items[$key]->linen_qty = $li->qty;
    		$linen_items[$key]->action = '<a href="#use-linen-modal" data-toggle="modal" data-linen-line-id = "' . $li->linen_line_id . '" data-order-id = "' . $li->order_id . '"data-linen-qty="' . $li->qty . '" data-linen-code="' . $li->linen_code . '" data-linen-name="' . $li->linen_name . '" class="btn btn-warning use-linen-btn"> Pakai</a>';
    	}

    	return response()->json($linen_items);
    }

    public function useLinen(Request $request)
    {
        $use_linen = new UseLinen;
        $use_linen->order_id = $request->modal_use_linen_order_id;
        $use_linen->linen_line_id = $request->modal_linen_line_id;
        $use_linen->linen_code = $request->modal_linen_code;
        $use_linen->linen_name = $request->modal_linen_name;
        $use_linen->qty = -1;
        $use_linen->status = 'in_use';
        $use_linen->created_by = Auth::user()->username;
        $use_linen->save();

        $wash_linen = new WashLinen;
        $wash_linen->order_id = $request->modal_use_linen_order_id;
        $wash_linen->linen_line_id = $request->modal_linen_line_id;
        $wash_linen->linen_code = $request->modal_linen_code;
        $wash_linen->linen_name = $request->modal_linen_name;
        $wash_linen->qty = 1;
        $wash_linen->status = 'in_use';
        $wash_linen->created_by = Auth::user()->username;
        $wash_linen->save();

        $order_linen_line = OrderLinenLine::find($request->modal_linen_line_id);
        $order_linen_line->used_qty += 1;
        $order_linen_line->save();

        $linen_in_use = Linens::find($order_linen_line->linen_id);
        $linen_in_use->frequencies_number += 1;
        $linen_in_use->save();

        return redirect ('/uselinen')->with('message', '<div class="alert alert-success alert-dismissible"> Transaksi berhasil.</div>');
    }

    public function useOrderLinen($order_id)
    {
        $use_order_linen_line = OrderLinenLine::where('order_linen_id', $order_id)
                                                ->where('used_qty', '<', 1)
                                                ->get();

        foreach ($use_order_linen_line as $key => $uoll) {
            $use_order_linen_line[$key]->qty_to_use = $uoll->linen_qty - $uoll->used_qty;
        }

        $data['linen_item'] = $use_order_linen_line;
        return response()->json($data);
    }

    public function useOrderSubmit(Request $request)
    {
        $order_id = $request->order_id;
        $linen_items = $request->linen_items;

        foreach ($linen_items as $key => $li) {
            $new_li = explode("|", $li);
            if ($new_li[3] == "true") {
                $use_linen = new UseLinen;
                $use_linen->order_id = $order_id;
                $use_linen->linen_line_id = $new_li[0];
                $use_linen->linen_code = $new_li[1];
                $use_linen->linen_name = $new_li[2];
                $use_linen->qty = -1;
                $use_linen->status = 'in_use';
                $use_linen->created_by = Auth::user()->username;
                $save_use_linen = $use_linen->save();

                $wash_linen = new WashLinen;
                $wash_linen->order_id = $order_id;
                $wash_linen->linen_line_id = $new_li[0];
                $wash_linen->linen_code = $new_li[1];
                $wash_linen->linen_name = $new_li[2];
                $wash_linen->qty = 1;
                $wash_linen->status = 'in_use';
                $wash_linen->created_by = Auth::user()->username;
                $save_wash_linen = $wash_linen->save();

                $order_linen_line = OrderLinenLine::find($new_li[0]);
                $order_linen_line->used_qty += 1;
                $save_order_linen_line = $order_linen_line->save();

                $linen_in_use = Linens::find($order_linen_line->linen_id);
                $linen_in_use->frequencies_number = 1;
                $linen_in_use->save();
            }
        }

        return response()->json(array('status' => 'success'));
    }
}
