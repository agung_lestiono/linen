<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Rooms;
use App\User;

class RoomController extends Controller
{
    public function index()
    {
        return view('RoomList');
    }

    public function getList()
    {
        $rooms = Rooms::all();

        foreach ($rooms as $key => $room) {
            $user = User::find($room->officer_id);

            $rooms[$key]->no = $key + 1;
            $rooms[$key]->officer = ($user) ? $user->name: '';
            $rooms[$key]->action = '<a href="/room/' . $room->id . '" class="btn btn-info"> <i class="fas fa-info-circle"></i></a>';
            $rooms[$key]->action .= '<a href="/room/delete/'. $room->id . '" class="btn btn-danger" onclick="if(!confirm(\'Anda yakin ingin menghapus ruangan ini?\')) return false;"> <i class="far fa-trash-alt"></i> </a>';
        }
        return response()->json($rooms);
    }

    public function create()
    {
        $user_list = array('' => ' -- Pilih Petugas -- ', );
        $users = User::where('role', '!=','superadmin')->get();
        foreach ($users as $key => $us) {
            $user_list[$us->id] = $us->name;
        }

        return view('roomForm', 
            [
                'pagetitle'     => 'Tambah Ruangan',
                'edit_mode'     => 1,
                'id'            => '',
                'name'          => '',
                'officer_list'  => $user_list,
                'officer_id'    => '',
            ]
        );
    }

    public function detail($id)
    {
        $room = Rooms::find($id);

        $user_list = array('' => ' -- Pilih Petugas -- ', );
        $users = User::where('role', '!=','superadmin')->get();
        foreach ($users as $key => $us) {
            $user_list[$us->id] = $us->name;
        }

        return view('roomForm', [
            'pagetitle'     => 'Detail Ruangan',
            'edit_mode'     => 0,
            'id'            => $room->id,
            'name'          => $room->name,
            'officer_list'  => $user_list,
            'officer_id'    => $room->officer_id,
        ]);
    }

    public function store(Request $request)
    {
        $id = $request->id;
        
        if ($id) {
            $room = Rooms::find($id);
            $room->updated_by = Auth::user()->name;
        }
        else {
            $room = new Rooms;
            $room->created_by = Auth::user()->name;
        }

        $room->name = $request->name;
        $room->officer_id = $request->officer_id;
        $room_store = $room->save();

        if ($room_store) {
            if ($id) {
                return redirect ('/room')->with('message', '<div class="alert alert-success alert-dismissible"> Ruangan berhasil diubah.</div>');
            } 
            else {
                return redirect ('/room')->with('message', '<div class="alert alert-success alert-dismissible"> Ruangan berhasil ditambah.</div>');
            }
        }
        else {
            return redirect ('/room')->with('message', '<div class="alert alert-danger alert-dismissible"> Gagal.</div>');
        }
    }

    public function delete($id)
    {
        $room = Rooms::find($id);
        $room_delete = $room->delete();

        if ($room_delete) {
            return redirect ('/room')->with('message', '<div class="alert alert-warning alert-dismissible"> Ruangan berhasil dihapus.</div>');
        }
        else {
            return redirect ('/room')->with('message', '<div class="alert alert-danger alert-dismissible"> Ruangan gagal dihapus.</div>');
        }

    }
}
