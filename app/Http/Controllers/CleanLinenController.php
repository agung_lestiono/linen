<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\OrderLinen;
use App\OrderLinenLine;
use App\WashLinen;
use App\CleanLinen;
use App\Linens;


class CleanLinenController extends Controller
{
    public function index()
    {
    	return view('cleanLinenList');
    }

    public function getList()
    {
    	$linen_items = CleanLinen::select('linen_line_id', 'order_id','linen_code', 'linen_name')
                                ->selectRaw('sum(qty) as qty')
                                ->groupBy('linen_line_id','order_id','linen_line_id', 'linen_code', 'linen_name')
                                ->havingRaw('sum(qty)>0')
                                ->get();

        foreach ($linen_items as $key => $li) {
            $order = OrderLinen::find($li->order_id);
            $linen_items[$key]->no = $key + 1;
            $linen_items[$key]->order_no = '<a href="#clean-order-modal" data-toggle="modal" data-modal-order-id="' . $li->order_id . '" data-modal-order-no="' . $order->order_no. '" class="clean-order-linen">' . $order->order_no . '</a>';
            $linen_items[$key]->linen_qty = $li->qty;
            $linen_items[$key]->action = '<a href="#clean-linen-modal" data-toggle="modal" data-linen-line-id = "' . $li->linen_line_id . '" data-order-id = "' . $li->order_id . '"data-linen-qty="' . $li->qty . '" data-linen-code="' . $li->linen_code . '" data-linen-name="' . $li->linen_name . '" class="btn btn-warning clean-linen-btn"> restock</a>';
        }

    	return response()->json($linen_items);
    }

    public function restock(Request $request)
    {
        $clean_linen = new CleanLinen;
        $clean_linen->order_id = $request->modal_use_linen_order_id;
        $clean_linen->linen_line_id = $request->modal_linen_line_id;
        $clean_linen->linen_code = $request->modal_linen_code;
        $clean_linen->linen_name = $request->modal_linen_name;
        $clean_linen->qty = -1;
        $clean_linen->status = 'done';
        $clean_linen->created_by = Auth::user()->username;
        $clean_linen->save();

        $order_linen_line = OrderLinenLine::find($request->modal_linen_line_id);
        $order_linen_line->clean_qty = 1;
        $order_linen_line->save();

        $linen = Linens::find($order_linen_line->linen_id);
        $linen->in_use = 0;
        $linen->qty = 1;
        $linen->save();

        $order_linen = OrderLinen::find($request->modal_use_linen_order_id);
        $order_linen_id = $order_linen->id;
        $order_linen_lines = OrderLinenLine::where('order_linen_id', $order_linen_id)->get();
        $diff_qty = 0;
        foreach ($order_linen_lines as $key => $oll) {
            $diff_qty += $oll->linen_qty - $oll->clean_qty;
        }

        if ($diff_qty == 0) {
            OrderLinen::where('id', $order_linen_id)->update(['status'=>'done', 'updated_by'=>Auth::user()->username]);
        }

        return redirect ('/cleanlinen')->with('message', '<div class="alert alert-success alert-dismissible"> Transaksi berhasil.</div>');
    }

    public function cleanOrderLinen($order_id)
    {
        $clean_order_linen_line = OrderLinenLine::where('order_linen_id', $order_id)
                                    ->where('wash_qty', 1)
                                    ->where('clean_qty',0)
                                    ->get();

        foreach ($clean_order_linen_line as $key => $coll) {
            $clean_order_linen_line[$key]->qty_to_use = $coll->linen_qty - $coll->wash_qty;
        }

        $data['linen_item'] = $clean_order_linen_line;
        return response()->json($data);
    }

    public function cleanOrderSubmit(Request $request)
    {
        $order_id = $request->order_id;
        $linen_items = $request->linen_items;

        foreach ($linen_items as $key => $li) {
            $new_li = explode("|", $li);
            if ($new_li[3] == "true") {
                $clean_linen = new CleanLinen;
                $clean_linen->order_id = $order_id;
                $clean_linen->linen_line_id = $new_li[0];
                $clean_linen->linen_code = $new_li[1];
                $clean_linen->linen_name = $new_li[2];
                $clean_linen->qty = -1;
                $clean_linen->status = 'done';
                $clean_linen->created_by = Auth::user()->username;
                $clean_linen->save();

                $order_linen_line = OrderLinenLine::find($new_li[0]);
                $order_linen_line->clean_qty = 1;
                $order_linen_line->save();

                $linen = Linens::find($order_linen_line->linen_id);
                $linen->in_use = 0;
                $linen->qty = 1;
                $linen->save();

                $order_linen_lines = OrderLinenLine::where('order_linen_id', $order_id)->get();
                $diff_qty = 0;
                foreach ($order_linen_lines as $key => $oll) {
                    $diff_qty += $oll->linen_qty - $oll->clean_qty;
                }

                if ($diff_qty == 0) {
                    OrderLinen::where('id', $order_id)->update(['status'=>'done', 'updated_by'=>Auth::user()->username]);
                }
            }
        }

        return response()->json(array('status' => 'success'));
    }
}
