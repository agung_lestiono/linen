<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\OrderLinen;
use App\OrderLinenLine;
use App\Linen;


class UseOrderController extends Controller
{
    public function index()
    {
    	return view('useOrderList');
    }

    public function getList()
    {
    	if (Auth::user()->role == 'superadmin' || Auth::user()->role == 'adminlinen') {
    		$orders = OrderLinen::where('status', 'approved')->get();
    	}
    	else {
    		$orders = OrderLinen::where('status', 'approved')
    							->where('created_by', Auth::user()->username)
    							->get();
    	}

    	foreach ($orders as $key => $o) {
    		$orders[$key]->no = $key + 1;
    		$orders[$key]->action = '<a href="/useorder/use/' . $o->id .'" class="btn btn-warning use-linen-btn"> Pakai</a>';
    	}

    	return response()->json($orders);
    }

    public function useOrder($id)
    {
        if ($id) {
            $order_linen_line = OrderLinenLine::where('order_linen_id', $id)
            									->get();

            foreach ($order_linen_line as $key => $oll) {
            	$linen = OrderLinenLine::where('id', $oll->id)->first();
            	$linen->used_qty = $linen->linen_qty;
            	$linen->updated_by = Auth::user()->username;
            	$linen->save();
            }

            OrderLinen::where('id', $id)
            			->update(['status'=>'done', 'updated_by'=>Auth::user()->username]);
            
            return redirect ('/useorder')->with('message', '<div class="alert alert-success alert-dismissible"> Transaksi berhasil.</div>');
        }
        else {
            return redirect ('/useorder')->with('message', '<div class="alert alert-danger alert-dismissible"> Gagal.</div>');  
        }
    }
}
