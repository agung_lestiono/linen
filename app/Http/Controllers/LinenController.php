<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Linens;
use App\GroupLinen;
use App\Rooms;
use App\UpdateStockHistory;

class LinenController extends Controller
{
    public function index()
    {
        return view('linenList');
    }

    public function getList()
    {
        $linens = Linens::all();

        foreach ($linens as $key => $linen) {
            $linens[$key]->no = $key + 1;
            $linens[$key]->action = '<a href="/linen/' . $linen->id . '" class="btn btn-info"> <i class="fas fa-info-circle"></i></a>';
            $linens[$key]->action .= '<a href="/linen/delete/'. $linen->id . '" class="btn btn-danger" onclick="if(!confirm(\'Anda yakin ingin menghapus linen ini?\')) return false;"> <i class="far fa-trash-alt"></i> </a>';

            if ($linen->broken == 1){
                $linens[$key]->linen_code = '<span style="color: red;">' . $linen->linen_code . '</span>';
                $linens[$key]->linen_name = '<span style="color: red;">' . $linen->linen_name . '</span>';
                $linens[$key]->uom = '<span style="color: red;">pcs</span>';
                $linens[$key]->frequencies_number = ($linen->frequencies_number > 0) ? '<span style="color: red;">' . $linen->frequencies_number . '</span>' : '<span style="color: red;">0</span>';
            }
            elseif ($linen->active == 0){
                $linens[$key]->linen_code = '<span style="color: #a6a6a6;">' . $linen->linen_code . '</span>';
                $linens[$key]->linen_name = '<span style="color: #a6a6a6;">' . $linen->linen_name . '</span>';
                $linens[$key]->uom = '<span style="color: #a6a6a6;">pcs</span>';
                $linens[$key]->frequencies_number = ($linen->frequencies_number > 0) ? '<span style="color: #a6a6a6;">' . $linen->frequencies_number . '</span>' : '<span style="color: red;">0</span>';
            }
            else {
                $linens[$key]->linen_code = $linen->linen_code;
                $linens[$key]->linen_name = $linen->linen_name;
                $linens[$key]->uom = 'pcs';
                $linens[$key]->frequencies_number = ($linen->frequencies_number > 0) ? $linen->frequencies_number  : 0;
            }
        }
        return response()->json($linens);
    }

    public function create()
    {
        $group_list = array('' => ' -- Pilih Grup -- ');
        $groups = GroupLinen::all();

        foreach ($groups as $key => $group) 
        {
            $group_list[$group->id] = $group->group_code . ' - ' . $group->group_name;
        }

        $room_list = array('' => ' -- Pilih Ruangan -- ');
        $rooms = Rooms::all();

        foreach ($rooms as $key => $room) 
        {
            $room_list[$room->id] = $room->name;
        }

        return view('linenForm', 
            [
                'pagetitle'     => 'Tambah Linen',
                'edit_mode'     => 1,
                'id'            => '',
                'linen_code'    => '',
                'group_list'    => $group_list,
                'group_id'      => '',
                'linen_name'    => '',
                'item_code'     => '',
                'room_list'     => $room_list,
                'room_id'       => '',
                'broken_qty'    => 0,
                'uom'           => '',
                'procurement_date' => date('d-m-Y'),
                'linen_age'     => '',
                'frequencies_number' => '',
            ]
        );
    }

    public function detail($id)
    {
        $linen = Linens::find($id);

        $group_list = array('' => ' -- Pilih Grup -- ');
        $groups = GroupLinen::all();

        foreach ($groups as $key => $group) 
        {
            $group_list[$group->id] = $group->group_code . ' - ' . $group->group_name;
        }

        $room_list = array('' => ' -- Pilih Ruangan -- ');
        $rooms = Rooms::all();

        foreach ($rooms as $key => $room) 
        {
            $room_list[$room->id] = $room->name;
        }

        $procurement_date = date_create($linen->procurement_date);
        $current_date = date_create(date('Y-m-d'));
        $linen_age = date_diff($procurement_date, $current_date);

        return view('linenForm', [
            'pagetitle'    => 'Detail Linen',
            'edit_mode'    => 0,
            'id'           => $linen->id,
            'group_list'   => $group_list,
            'group_id'     => $linen->group_id,
            'linen_name'   => $linen->linen_name,
            'linen_code'   => $linen->linen_code,
            'room_list'    => $room_list,
            'room_id'      => $linen->room_id,
            'uom'          => $linen->uom,
            'procurement_date'=> date('d-m-Y',strtotime($linen->procurement_date)),
            'linen_age'    => $linen_age->y . ' tahun ' . $linen_age->m . ' bulan ' . $linen_age->d . ' hari ',
            'date_diff'    => $linen_age->d * ($linen_age->y*365),
            'frequencies_number' => $linen->frequencies_number,
            'in_use'       => $linen->in_use,
            'active'       => $linen->active,
            'broken'       => $linen->broken,
            'broken_reason' => $linen->broken_reason,
        ]);
    }

    public function store(Request $request)
    {
        $id = $request->id;
        
        if ($id) {
            $linen = Linens::find($id);
            $linen->updated_by = Auth::user()->name;
        }
        else {
            $linen = new Linens;
            $linen->created_by = Auth::user()->name;
        }

        $linen->linen_code = $request->linen_code;
        $linen->group_id = $request->group_id;
        $linen->linen_name = $request->linen_name;
        $linen->room_id = $request->room_id;
        $linen->procurement_date = date('Y-m-d', strtotime($request->procurement_date));
        $linen->active = ($request->active == '') ? 0 :1;
        $linen->broken = ($request->broken == '') ? 0 :1;
        $linen->broken_reason = $request->broken_reason;
        $linen_store = $linen->save();

        if ($linen_store) {
            if ($id) {
                return redirect ('/linen')->with('message', '<div class="alert alert-success alert-dismissible"> Linen berhasil diubah.</div>');
            } 
            else {
                return redirect ('/linen')->with('message', '<div class="alert alert-success alert-dismissible"> Linen berhasil ditambahkan.</div>');
            }
        }
        else {
            return redirect ('/linen')->with('message', '<div class="alert alert-danger alert-dismissible"> Gagal.</div>');
        }
    }

    public function updateQty(Request $request)
    {
        Linens::where('id', $request->linen_id)->update(['qty'=> $request->new_stock_qty, 'updated_by'=> Auth::user()->username ]);
        $update = new UpdateStockHistory;
        $update->qty = $request->new_stock_qty;
        $update->linen_id = $request->linen_id;
        $update->reason = $request->reason;
        $update->qty_before = $request->qty_before;
        $update->save();

        return redirect('/linen/' . $request->linen_id);
    }

    public function delete($id)
    {
        $linen = Linens::find($id);
        $linen_delete = $linen->delete();

        if ($linen_delete) {
            return redirect ('/linen')->with('message', '<div class="alert alert-warning alert-dismissible"> Linen berhasil dihapus.</div>');
        }
        else {
            return redirect ('/linen')->with('message', '<div class="alert alert-danger alert-dismissible"> Linen gagal dihapus.</div>');
        }
    }

    public function getGroupData($id)
    {
        $group = GroupLinen::find($id);

        return response()->json($group);
    }
}
