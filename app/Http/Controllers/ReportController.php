<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;

use App\Linens;
use App\UseLinen;
use App\WashLinen;

use Excel;

class StockExport implements FromView
{
    public function view(): View
    {
        $stock_data = Linens::select('linen_name')
                        ->selectRaw('sum(qty) as stock_qty')
                        ->where('in_use', '!=', 1)
                        ->orderBy('linen_name')
                        ->groupBy('linen_name')
                        ->get();

        foreach ($stock_data as $key => $sd) {
            $stock_data[$key]->no = $key + 1;
            $stock_data[$key]->uom = 'pcs';
        }

        return view('exportLinen', [
            'type'       => 'group',
            'stock_data' => $stock_data
        ]);
    }
}

class StockExportDetails implements FromView
{
    public function view(): View
    {
        $detail_stock_data = Linens::where('in_use', '!=', 1)
                        ->orderBy('linen_code')
                        ->get();

        foreach ($detail_stock_data as $key => $sd) {
            $detail_stock_data[$key]->no = $key + 1;
            $detail_stock_data[$key]->uom = 'pcs';
        }

        return view('exportLinen', [
            'type'              => 'detail',
            'detail_stock_data' => $detail_stock_data
        ]);
    }
}

class UseExport implements FromView
{
    public function view(): View
    {
        $used_linen = UseLinen::select('linen_name')
                                ->selectRaw('sum(qty) as qty')
                                ->havingRaw('sum(qty)>0')
                                ->groupBy('linen_name')
                                ->orderBy('linen_name')
                                ->get();

        foreach ($used_linen as $key => $ul) {
            $used_linen[$key]->no = $key + 1;
            $used_linen[$key]->uom = 'pcs';
        }

        return view('exportUse', [
            'used_linen' => $used_linen
        ]);
    }
}

class WashExport implements FromView
{
    public function view(): View 
    {
        $washed_linen = WashLinen::select('linen_name')
                                    ->selectRaw('sum(qty) as qty')
                                    ->havingRaw('sum(qty)>0')
                                    ->groupBy('linen_name')
                                    ->orderBy('linen_name')
                                    ->get();

        foreach ($washed_linen as $key => $wl) {
            $washed_linen[$key]->no = $key + 1;
            $washed_linen[$key]->uom = pcs;
        }

        return view('exportWashLinen', [
            'washed_linen' => $washed_linen
        ]);
    }
}

class BrokenExport implements FromView
{
    public function view(): View
    {
        $broken_linens = Linens::where('broken', 1)->orderBy('linen_code')->get();
        foreach ($broken_linens as $key => $bl) {
            $broken_linens[$key]->no = $key + 1;
            $broken_linens[$key]->qty = 1;
            $broken_linens[$key]->uom = 'pcs';
        }

        return view('exportBrokenLinen', [
            'format'       => 'detail',
            'broken_linen' => $broken_linens
        ]);
    }
}

class GroupBrokenExport implements FromView
{
    public function view(): View
    {
        $broken_linens = Linens::select('linen_name')
                            ->selectRaw('sum(qty) as qty')
                            ->where('broken', 1)
                            ->groupBy('linen_name')
                            ->orderBy('linen_code')
                            ->get();

        foreach ($broken_linens as $key => $bl) {
            $broken_linens[$key]->no = $key + 1;
            $broken_linens[$key]->qty = 1;
            $broken_linens[$key]->uom = 'pcs';
        }

        return view('exportBrokenLinen', [
            'format'       => 'group',
            'broken_linen' => $broken_linens
        ]);
    }
    
}

class ReportController extends Controller
{
    public function stock()
    {
    	return view('reportStock');
    }

    public function getStock()
    {
    	$stocks = Linens::select('linen_name')
                        ->selectRaw('sum(qty) as stock_qty')
                        ->where('in_use', '!=', 1)
                        ->orderBy('linen_name')
                        ->groupBy('linen_name')
                        ->get();

    	foreach ($stocks as $key => $s) {
    		$stocks[$key]->no = $key + 1;
            $stocks[$key]->uom = 'pcs';
    	}

    	return response()->json($stocks);
    }

    public function exportStock()
    {
        return Excel::download(new StockExport, 'Persediaan Linen.xlsx');
    }

    public function exportStockDetail()
    {
        return Excel::download(new StockExportDetails, 'Detail Persediaan Linen.xlsx');
    }

    public function useLinen()
    {
    	return view('reportUseLinen');
    }

    public function getUseLinen()
    {
        $used_linen = UseLinen::select('linen_name')
                                ->selectRaw('sum(qty) as qty')
                                ->havingRaw('sum(qty)>0')
                                ->groupBy('linen_name')
                                ->orderBy('linen_code')
                                ->get();

        foreach ($used_linen as $key => $ul) {
            $used_linen[$key]->no = $key + 1;
            $used_linen[$key]->uom = 'pcs';
        }

        return json_encode($used_linen);
    }

    public function exportUse()
    {
        return Excel::download(new UseExport, 'Linen Digunakan.xlsx');
    }

    public function washlinen()
    {
    	return view('reportWashLinen');
    }

    public function getWashLinen()
    {
        $washed_linen = WashLinen::select('linen_code', 'linen_name')
                                    ->selectRaw('sum(qty) as qty')
                                    ->havingRaw('sum(qty)>0')
                                    ->groupBy('linen_code', 'linen_name')
                                    ->orderBy('linen_code')
                                    ->get();

        foreach ($washed_linen as $key => $wl) {
            $washed_linen[$key]->no = $key + 1;
            $washed_linen[$key]->uom = pcs;
        }

        return json_encode($washed_linen);
    }

    public function exportWash()
    {
        return Excel::download(new WashExport, 'Linen Dicuci.xlsx');
    }

    public function broken()
    {
        return view('reportBrokenLinen');
    }

    public function getBroken()
    {
        $broken_linens = Linens::where('broken', 1)->orderBy('linen_code')->get();
        foreach ($broken_linens as $key => $bl) {
            $broken_linens[$key]->no = $key + 1;
            $broken_linens[$key]->qty = 1;
            $broken_linens[$key]->uom = 'pcs';
        }

        return response()->json($broken_linens);
    }

    public function exportBroken()
    {
        return Excel::download(new BrokenExport, 'Linen Rusak.xlsx');
    }

}
