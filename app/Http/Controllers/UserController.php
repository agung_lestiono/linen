<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use App\User;

class UserController extends Controller
{
    public function index()
    {
        return view('userList');
    }

    public function getList()
    {
        $users = User::all();

        foreach ($users as $key => $user) {
            if ($user->role == 'superadmin') {
                $role = 'Super Administrator';
            }
            elseif ($user->role == 'admin_linen') {
                $role = 'Admin Linen';
            }
            elseif ($user->role == 'admin_room') {
                $role = 'Admin Ruangan';
            }
            elseif ($user->role == 'admin_laundry') {
                $role = 'Admin Laundry';
            }
            else {
                $role = '';
            }

            $users[$key]->no = $key + 1;
            $users[$key]->role = $role;
            $users[$key]->action = '<a href="/user/' . $user->id . '" class="btn btn-info"> <i class="fas fa-info-circle"></i></a>';

            if ($user->id != 1)
                $users[$key]->action .= '<a href="/user/delete/'. $user->id . '" class="btn btn-danger" onclick="if(!confirm(\'Anda yakin ingin menghapus pengguna ini?\')) return false;"> <i class="far fa-trash-alt"></i> </a>';
        }
        return response()->json($users);
    }

    public function create()
    {
        return view('userForm', 
            [
                'pagetitle'     => 'Tambah Pengguna',
                'id'            => '',
                'name'          => '',
                'email'         => '',
                'username'      => '',
                'role'          => '',
                'password'      => ''
            ]
        );
    }

    public function detail($id)
    {
        $user = User::find($id);

        return view('userForm', [
            'pagetitle'     => 'Detail Pengguna',
            'id'            => $user->id,
            'name'          => $user->name,
            'email'         => $user->email,
            'username'      => $user->username,
            'role'          => $user->role,
            'password'      => '',

        ]);
    }

    public function store(Request $request)
    {
        $id = $request->id;
        
        if ($id) {
            $user = User::find($id);
            $user->updated_by = Auth::user()->name;
        }
        else {
            $user = new User;
            $user->created_by = Auth::user()->name;
        }

        if ($request->password) {
            $user->password = Hash::make($request->password);
        }

        $user->name = $request->name;
        $user->email = $request->email;
        $user->username = $request->username;
        $user->role = $request->role;
        $user_store = $user->save();

        if ($user_store) {
             if ($id) {
                return redirect ('/user')->with('message', '<div class="alert alert-success alert-dismissible"> Pengguna berhasil diubah.</div>');
            } 
            else {
                return redirect ('/user')->with('message', '<div class="alert alert-success alert-dismissible"> Pengguna berhasil ditambah.</div>');
            }
        }
        else {
            return redirect ('/user')->with('message', '<div class="alert alert-danger alert-dismissible"> Gagal.</div>');
        }
    }

    public function delete($id)
    {
        $user = User::find($id);
        $user_delete = $user->delete();

        if ($user_delete) {
            return redirect ('/room')->with('message', '<div class="alert alert-warning alert-dismissible"> Pengguna berhasil dihapus.</div>');
        }
        else {
            return redirect ('/room')->with('message', '<div class="alert alert-danger alert-dismissible"> Penggun gagal dihapus.</div>');
        }

    }
}
