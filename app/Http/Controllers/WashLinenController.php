<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\OrderLinen;
use App\OrderLinenLine;
use App\WashLinen;
use App\CleanLinen;
use App\Linens;


class WashLinenController extends Controller
{
    public function index()
    {
    	return view('washLinenList');
    }

    public function getList()
    {
    	$linen_items = WashLinen::select('linen_line_id', 'order_id','linen_code', 'linen_name')
                                ->selectRaw('sum(qty) as qty')
                                ->groupBy('linen_line_id','order_id','linen_line_id', 'linen_code', 'linen_name')
                                ->havingRaw('sum(qty)>0')
                                ->get();

        foreach ($linen_items as $key => $li) {
            $order = OrderLinen::find($li->order_id);
            $linen_items[$key]->no = $key + 1;
            $linen_items[$key]->order_no = '<a href="#wash-order-modal" data-toggle="modal" data-modal-order-id="' . $li->order_id . '" data-modal-order-no="' . $order->order_no. '" class="wash-order-linen">' . $order->order_no . '</a>';
            $linen_items[$key]->linen_qty = $li->qty;
            $linen_items[$key]->action = '<a href="#wash-linen-modal" data-toggle="modal" data-linen-line-id = "' . $li->linen_line_id . '" data-order-id = "' . $li->order_id . '"data-linen-qty="' . $li->qty . '" data-linen-code="' . $li->linen_code . '" data-linen-name="' . $li->linen_name . '" class="btn btn-warning wash-linen-btn"> Cuci</a>';
        }

    	return response()->json($linen_items);
    }

    public function washLinen(Request $request)
    {
        $wash_linen = new WashLinen;
        $wash_linen->order_id = $request->modal_use_linen_order_id;
        $wash_linen->linen_line_id = $request->modal_linen_line_id;
        $wash_linen->linen_code = $request->modal_linen_code;
        $wash_linen->linen_name = $request->modal_linen_name;
        $wash_linen->qty = -1;
        $wash_linen->status = 'wash';
        $wash_linen->created_by = Auth::user()->username;
        $wash_linen->save();

        $clean_linen = new CleanLinen;
        $clean_linen->order_id = $request->modal_use_linen_order_id;
        $clean_linen->linen_line_id = $request->modal_linen_line_id;
        $clean_linen->linen_code = $request->modal_linen_code;
        $clean_linen->linen_name = $request->modal_linen_name;
        $clean_linen->qty = 1;
        $clean_linen->status = 'ready_to_restock';
        $clean_linen->created_by = Auth::user()->username;
        $clean_linen->save();

        $order_linen_line = OrderLinenLine::find($request->modal_linen_line_id);
        $order_linen_line->wash_qty += $request->modal_linen_qty;
        $order_linen_line->save();

        return redirect ('/washlinen')->with('message', '<div class="alert alert-success alert-dismissible"> Transaksi berhasil.</div>');
    }

    public function washOrderLinen($order_id)
    {
        $use_order_linen_line = OrderLinenLine::where('order_linen_id', $order_id)
                                    ->where('used_qty', 1)
                                    ->where('wash_qty', 0)
                                    ->get();

        foreach ($use_order_linen_line as $key => $uoll) {
            $use_order_linen_line[$key]->qty_to_use = $uoll->linen_qty - $uoll->wash_qty;
        }

        $data['linen_item'] = $use_order_linen_line;
        return response()->json($data);
    }

    public function washOrderSubmit(Request $request)
    {
        $order_id = $request->order_id;
        $linen_items = $request->linen_items;

        foreach ($linen_items as $key => $li) {
            $new_li = explode("|", $li);
            if ($new_li[3] == "true") {
                $wash_linen = new WashLinen;
                $wash_linen->order_id = $order_id;
                $wash_linen->linen_line_id = $new_li[0];
                $wash_linen->linen_code = $new_li[1];
                $wash_linen->linen_name = $new_li[2];
                $wash_linen->qty = -1;
                $wash_linen->status = 'wash';
                $wash_linen->created_by = Auth::user()->username;
                $wash_linen->save();

                $clean_linen = new CleanLinen;
                $clean_linen->order_id = $order_id;
                $clean_linen->linen_line_id = $new_li[0];
                $clean_linen->linen_code = $new_li[1];
                $clean_linen->linen_name = $new_li[2];
                $clean_linen->qty = 1;
                $clean_linen->status = 'ready_to_restock';
                $clean_linen->created_by = Auth::user()->username;
                $clean_linen->save();

                $order_linen_line = OrderLinenLine::find($new_li[0]);
                $order_linen_line->wash_qty = 1;
                $order_linen_line->save();
            }
        }

        return response()->json(array('status' => 'success'));
    }
}
