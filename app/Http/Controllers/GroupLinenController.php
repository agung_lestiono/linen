<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\GroupLinen;

class GroupLinenController extends Controller
{
	public function index()
    {
        return view('groupLinenList');
    }

    public function getList()
    {
        $groups = GroupLinen::all();

        foreach ($groups as $key => $group) {
            $groups[$key]->no = $key + 1;
            $groups[$key]->action = '<a href="/grouplinen/' . $group->id . '" class="btn btn-info"> <i class="fas fa-info-circle"></i></a>';
            $groups[$key]->action .= '<a href="/grouplinen/delete/'. $group->id . '" class="btn btn-danger" onclick="if(!confirm(\'Anda yakin ingin menghapus grup ini?\')) return false;"> <i class="far fa-trash-alt"></i> </a>';
        }
        return response()->json($groups);
    }

    public function create()
    {
        return view('groupLinenForm', 
            [
                'pagetitle' => 'Tambah Grup Linen',
                'edit_mode' => 1,
                'id' => '',
                'group_code' => '',
                'group_name' => '',
            ]
        );
    }

    public function detail($id)
    {
        $group = GroupLinen::find($id);

        return view('groupLinenForm', [
            'pagetitle' => 'Detail Grup Linen',
            'edit_mode' => 0,
            'id' => $group->id,
            'group_code' => $group->group_code,
            'group_name' => $group->group_name,
        ]);
    }

    public function store(Request $request)
    {
        $id = $request->id;
        // print_r(Auth::user()->name);exit();
        if ($id) {
            $group = GroupLinen::find($id);
            $group->updated_by = Auth::user()->name;
        }
        else {
            $group = new GroupLinen;
            $group->created_by = Auth::user()->name;
        }

        $group->group_name = $request->group_name;
        $group->group_code = $request->group_code;
        $group_store = $group->save();

        if ($group_store) {
            if ($id) {
                return redirect ('/grouplinen')->with('message', '<div class="alert alert-success alert-dismissible"> Grup berhasil diubah.</div>');
            } 
            else {
                return redirect ('/grouplinen')->with('message', '<div class="alert alert-success alert-dismissible"> Grup berhasil ditambah.</div>');
            }
        }
        else {
            return redirect ('/grouplinen')->with('message', '<div class="alert alert-danger alert-dismissible"> Gagal.</div>');
        }
    }

    public function delete($id)
    {
        $group = GroupLinen::find($id);
        $group_delete = $group->delete();

        if ($group_delete) {
            return redirect ('/grouplinen')->with('message', '<div class="alert alert-warning alert-dismissible"> Grup berhasil dihapus.</div>');
        }
        else {
            return redirect ('/grouplinen')->with('message', '<div class="alert alert-danger alert-dismissible"> Grup gagal dihapus.</div>');
        }

    }
}
