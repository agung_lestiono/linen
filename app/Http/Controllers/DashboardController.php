<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Linens;
use App\OrderLinen;
use App\OrderLinenLine;
use App\Rooms;

class DashboardController extends Controller
{
    public function index()
    {
     //    $current_date = date('Y-m-d');
     //    $date_aweek_before = date('Y-m-d', strtotime('-1 week'));

    	// $often_used_linen = OrderLinenLine::select('linen_code')
    	// 									->selectRaw('sum(linen_qty)')
    	// 									->groupBy('linen_code')
    	// 									->take(5)
    	// 									->get();
    	
    	// return view('dashboard', [
     //        // 'date_in_week' => $date_in_week,
     //        // 'order_in_week' => $order_in_week,
    	// 	'top_linen' => $often_used_linen[0],
    	// ]);

        return view('dashboard');
    }

    public function getDataOrder()
    {
        $current_date = date('Y-m-d');
        $date_aweek_before = date('Y-m-d', strtotime('-1 week'));

        $data['date_in_week'] = [];
        $data['order_in_week'] = [];

        for ($i=0; $i < 7; $i++) { 
            array_push($data['date_in_week'], date('d F', strtotime($date_aweek_before . '+'. $i .' day')));
            $order_per_day = OrderLinenLine::leftJoin('order_linen', 'order_linen.id', 'order_linen_line.order_linen_id')
                                        ->where('order_linen.order_date', date('Y-m-d', strtotime($date_aweek_before . '+'. $i .' day')))
                                        ->count('order_linen_line.id');
            array_push($data['order_in_week'], $order_per_day);
        }

        return json_encode($data);
    }

    public function getDataLinen()
    {
        $linens = Linens::orderBy('frequencies_number', 'desc')
                        ->take(7)
                        ->get();

        $data['linen_code'] = [];
        $data['linen_freq'] = [];

        foreach ($linens as $key => $l) {
            array_push($data['linen_code'], $l->linen_code);
            array_push($data['linen_freq'], $l->frequencies_number);
        }

        if ($data['linen_code'] == []) {
            $linens = Linens::take(7)->orderBy('linen_code')->get();
            foreach ($linens as $key => $l) {
                array_push($data['linen_code'], $l->linen_code);
                array_push($data['linen_freq'], 0);
            }
        }
        return json_encode($data);
    }
}
