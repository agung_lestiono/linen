<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CleanLinen extends Model
{
    protected $table = 'clean_linen';
}
