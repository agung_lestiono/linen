<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderLinen extends Model
{
    protected $table = 'order_linen';
}
