<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UseLinen extends Model
{
    protected $table = 'use_linen';
}
