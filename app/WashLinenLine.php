<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WashLinenLine extends Model
{
    protected $table = 'wash_linen_line';
}
