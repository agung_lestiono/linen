<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UpdateStockHistory extends Model
{
    protected $table = 'update_stock_history';
}
