<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderLinenLine extends Model
{
    protected $table = 'order_linen_line';
}
