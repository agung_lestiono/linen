<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WashLinen extends Model
{
    protected $table = 'wash_linen';
}
